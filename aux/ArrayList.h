#ifndef HEADER_ARRAYLIST
#define HEADER_ARRAYLIST

typedef struct ArrayList {
    size_t size;
    void ** data;
};

ArrayList* ArrayList_alloc();
void ArrayList_add(ArrayList *list, double element);
void ArrayList_set(ArrayList *list, double element);
double  ArrayList_get(ArrayList *list, int i);

#endif
