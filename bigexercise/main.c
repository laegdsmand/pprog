#include<math.h>
#include<stdio.h>
#include<assert.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_sf_ellint.h>


double integrand(double t, void * params){
	double k = *(double*) params;
	return sqrt(1 - k*k*pow(sin(t),2));
}


double E(double phi, double k){
	if(phi > M_PI){
		return E(M_PI,k) + E(phi-M_PI,k);
	}
	if(phi < 0){
		return -E(-phi,k);
	}	
	size_t n_intervals = 512;
	double epsrel = 1e-7, result, abserr;
	gsl_integration_workspace * w = gsl_integration_workspace_alloc(n_intervals);
	gsl_function F = {.function = &integrand, .params = &k};
	gsl_integration_qags(&F, 0, phi,
		0, epsrel, n_intervals, w, &result, &abserr);
	gsl_integration_workspace_free(w);
	return result;
}




void plot(double k){
	double phi_min = -M_PI, phi_max = 2*M_PI;
	int n_steps = 100;
	for(int i = 0; i <= n_steps; i++){
		double phi = (phi_max-phi_min) / n_steps * i + phi_min;
		double E_result = E(phi, k);
		double E_expect = gsl_sf_ellint_E(phi,k,GSL_PREC_DOUBLE);
		printf("%lg\t%lg\t%lg\n",phi,E_result,E_expect);
	}
}

int main(){
	plot(0.9);	
	printf("\n\n");
	plot(0.7);
	printf("\n\n");
	plot(0.5);
	return 0;
}




