#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multiroots.h>

struct rosenbrock_params {
	double a;
	double b;
};

int f_rosenbrock(const gsl_vector * x, void *params, gsl_vector* f){
	double a = ((struct rosenbrock_params *) params) -> a; 
	double b = ((struct rosenbrock_params *) params) -> b; 
	
	const double x0 = gsl_vector_get(x,0);
	const double x1 = gsl_vector_get(x,1);
	

	//Calculates the gradient of the rosenbrock function
	//g_x = -2(1-x) + 2*100*(y-x²)*2x
	//g_y = 2*100*(y-x²)*1
	const double g0 = -2*a*(1-x0) + 4*b*(x1-x0*x0)*x0;
	const double g1 = 2*b*(x1-x0*x0);

	gsl_vector_set(f,0,g0);
	gsl_vector_set(f,1,g1);


	return GSL_SUCCESS;
}

void print_state(size_t iter, gsl_multiroot_fsolver * s){
	printf("Iter = %3u, x = %.3f, y = %.3f, f(x) = (%.3e, %.3e)\n",
		iter,
		gsl_vector_get(s->x,0),
		gsl_vector_get(s->x,1),
		gsl_vector_get(s->f,0),
		gsl_vector_get(s->f,1)
	);

}

int main(){


	const gsl_multiroot_fsolver_type *T = 
		gsl_multiroot_fsolver_hybrids;

	gsl_multiroot_fsolver *s = gsl_multiroot_fsolver_alloc(T,2);

	size_t i, iter = 0;
	const size_t n = 2;
	struct rosenbrock_params params = {1.0,100.0};
	gsl_multiroot_function f = {&f_rosenbrock,n,&params};
	
	double x_init[2] = {1.5, 2.0};
	
	gsl_vector* x = gsl_vector_alloc(n);
	gsl_vector_set(x,0,x_init[0]);
	gsl_vector_set(x,1,x_init[1]);
	
	gsl_multiroot_fsolver_set(s,&f,x);	
	print_state(iter,s);
	int status;
	do{
		iter++;
		status = gsl_multiroot_fsolver_iterate(s);
		print_state(iter,s);
		if(status) break;
		status = gsl_multiroot_test_residual(s->f, 1e-7);

	}while(status == GSL_CONTINUE && iter < 1000);	
	
	printf("status = %s\n",gsl_strerror(status));
	gsl_multiroot_fsolver_free(s);
	gsl_vector_free(x);
	
	return 0;
}




