#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_vector.h>
#include<stdio.h>
#include<math.h>
#include<assert.h>


int ode_H(double r, const double y[], double dydt[], void *params);
int  M_fun(const gsl_vector *x, void *params, gsl_vector *f);

int ode_H(double r, const double y[], double dydt[], void *params){
	double epsilon = *(double*) params;
	dydt[0] = y[1];
	dydt[1] = -2*(y[0]/r + epsilon * y[0]);
	return GSL_SUCCESS;
} 

double solveDifferentialEq(double epsilon, double r){
	gsl_odeiv2_system sys = {ode_H, NULL, 2, &epsilon};
	const gsl_odeiv2_step_type * T = gsl_odeiv2_step_rkf45;	
	gsl_odeiv2_driver* driver = 
		gsl_odeiv2_driver_alloc_y_new(&sys, T, 1e-6, 1e-6, 0.0);
	int N_steps = 200;
	double t_init = 1e-3, t = t_init;
	double t_final = r;
	double y[2] = {t-t*t, 1-2*t}; //Set the initial value of y
	int ode_status = gsl_odeiv2_driver_apply(driver, &t, r, y);
	if(ode_status != GSL_SUCCESS) fprintf(stderr,"odeiv2 error: %d\n",ode_status);
	gsl_odeiv2_driver_free(driver);
	return y[0];
}

int M_fun(const gsl_vector *x, void *params, gsl_vector *f){
	double epsilon = gsl_vector_get(x,0);
	double r_max = *(double*) params;
	double f_res = solveDifferentialEq(epsilon,r_max);
	gsl_vector_set(f,0,f_res);
	return GSL_SUCCESS;
}

int main(){
	double r_max = 8.0;
	int dim = 1;
	gsl_multiroot_fsolver * solver = gsl_multiroot_fsolver_alloc(
		gsl_multiroot_fsolver_broyden, dim);

	gsl_multiroot_function F = {.f=M_fun, .n=dim, .params = (void*) &r_max};

	gsl_vector *x = gsl_vector_alloc(dim);
	gsl_vector_set(x,0,-1.0);
	gsl_multiroot_fsolver_set(solver,&F,x);
	int status, iter = 0;
	
	do{
		iter++;
		status = gsl_multiroot_fsolver_iterate(solver);
		
		if(status) break;
		status = gsl_multiroot_test_residual(solver->f, 1e-3);
		
		fprintf(stderr,"iter= %3i ",iter);
		fprintf(stderr,"e= %10g ",gsl_vector_get(solver->x,0));
		fprintf(stderr,"f(rmax)= %10g ",gsl_vector_get(solver->f,0));
		fprintf(stderr,"\n");
	}while(status == GSL_CONTINUE && iter < 500);	
	double e_root = gsl_vector_get(solver->x,0);
	
	int steps = 100;
	for(int i = 1; i < steps; i++){
		double r = r_max/steps * i;
		printf("%g\t%g\t%g\n", r, solveDifferentialEq(e_root,r), r * exp(-r)); 
	}

	gsl_vector_free(x);
	
	gsl_multiroot_fsolver_free(solver);
		
}


