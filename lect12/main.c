#include<stdio.h>
#include<math.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_matrix.h>
//#define M_PI 3.14159265359

int ode_errfun(double x, const double y[], double f[], void *params){
	f[0] = (2/sqrt(M_PI))*exp(-x*x);
	return GSL_SUCCESS;
}

double error_function(double x){
	gsl_odeiv2_system ode_system;
	ode_system.function = ode_errfun;
	ode_system.jacobian = NULL;
	ode_system.dimension = 1;
	ode_system.params = NULL;
	double hstart = 1e-6;
	if(x<0) hstart = -hstart;
	double epsabs = 1e-6;
	gsl_odeiv2_driver* driver = 
		gsl_odeiv2_driver_alloc_y_new(&ode_system, gsl_odeiv2_step_rk8pd,hstart,epsabs,0.0);
	double y[1] = {0};
	double t = 0.0, t1 = x;
	gsl_odeiv2_driver_apply(driver,&t,t1,y);	
	gsl_odeiv2_driver_free(driver);
	return y[0];
}

void plot_erf(double a,double b, double dx){
	for(double x = a; x <= b; x+=dx){
		printf("%g\t%g\n",x,error_function(x));
	}
}

int main(int argc, char** argv){
	if(argc==4){
		double a = atof(argv[1]);
		double b = atof(argv[2]);
		double dx = atof(argv[3]);
		plot_erf(a,b,dx);
	}
	return 0;
}
