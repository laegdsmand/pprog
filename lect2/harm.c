#include "stdio.h"

double f(int x){
	return 1.0/x;
}

int main(){
	double s = 0;
	int n = 10000000;
	for(int i = 1; i <= n; i++){
		s += f(i);
	}
	printf("Sum=%f",s);
	return 0;
}
