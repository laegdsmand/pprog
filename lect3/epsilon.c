#include<limits.h>
#include<float.h>
#include<stdio.h>

int main(){
	printf("Epsilon exercise 1\n Part 1\n");
	int i = 1;
	while(i+1>i){
		i++;
	}
	printf("my max int =\t %i\n MAX_INT =\t %i\n",i,INT_MAX);
	for(i=1;i+1>i;i++);
	printf("Max int for loop \t = %i\n",i);
	i = 0;
	do{
		i++;	
	}while(i+1>i);
	printf("Max int do while \t = %i\n",i);
	
	i = 0;
	while(i-1<i){
		i--;
	}
	printf("\n Part 2\n");
	printf("Value of int min \t = %i\nint min from while loop  = %i\n",INT_MIN,i);
	for(int i = 0; i-1<i;i--);
	printf("int min from for loop \t = %i\n",i);
	i = 0;
	do{
		i--;
	}while(i-1<i);
	printf("int min from do while \t = %i\n",i);
	
	printf("\n Part 3 \n");
	printf("FLOAT EPSILON\t = %g\n",FLT_EPSILON);
	printf("DOUBLE EPSILON\t = %lg\n",DBL_EPSILON);
	printf("LDBL EPSILON\t = %Lg\n",LDBL_EPSILON);
	float x = 1;
	while(1+x != 1){
		x /= 2;
	}
	x*=2;
	printf("FLOAT EPSILON WHILE \t = %g\n",x);
	for(x = 1; 1+x != 1; x/=2);
	x *= 2;
	printf("FLOAT EPSILON FOR \t = %g\n",x);
	x = 1;
	do{
		x /= 2; 
	}while(x+1 != 1);
	x *= 2;
	printf("FLOAT EPSILON DOWHILE \t = %g\n",x);
	double y = 1;
	while(1+y != 1){
		y /= 2;
	}
	y*=2;
	printf("DOUBLE EPSILON WHILE \t = %lg\n",y);
	for(y = 1; 1+y != 1; y/=2);
	y *= 2;
	printf("DOUBLE EPSILON FOR \t = %lg\n",y);
	y = 1;
	do{
		y /= 2; 
	}while(y+1 != 1);
	y *= 2;
	printf("DOUBLE EPSILON DOWHILE \t = %lg\n",y);
	long double z = 1;
	while(1+z != 1){
		z /= 2;
	}
	z*=2;
	printf("LONG DOUBLE EPSILON WHILE \t = %Lg\n",z);
	for(z = 1; 1+z != 1; z/=2);
	z *= 2;
	printf("LONG DOUBLE EPSILON FOR \t = %Lg\n",z);
	z = 1;
	do{
		z /= 2; 
	}while(z+1 != 1);
	z *= 2;
	printf("LONG DOUBLE EPSILON DOWHILE \t = %Lg\n",z);
	
	return 0;
}
