#include<stdio.h>
#include<limits.h>
#include<float.h>

int main(){
	printf("\n Part 1\n");
	int max = INT_MAX/2;
	float sum_up_float = 0;
	for(int i = 1; i <= max; i++){
		sum_up_float += 1.0f/i;
	}
	float sum_down_float = 0;
	for(int i = max; i >= 1; i--){
		sum_down_float += 1.0f/i;
	}
	printf("FLOAT SUM UP = \t%f\n",sum_up_float);
	printf("FLOAT SUM DOWN =  %f\n",sum_down_float);
	printf("\n Part 2\n");
	printf("The difference in the two sums can be explained by the following: summing\n 		up we are adding the larger numbers first and then add comparatively smaller\n		numbers to it. Adding larger numbers and smaller numbers gives a larger error\n		than adding comparable numbers together which is what we are doing in\n			the down sum, so it should be more accurate");
	printf("\n Part 3\n");
	printf("No, the sum diverges as max -> inf");
	printf("\n Part 4\n");
	double sum_up_double = 0;
	for(int i = 1; i <= max; i++){
		sum_up_double += 1.0f/i;
	}
	double sum_down_double = 0;
	for(int i = max; i >= 1; i--){
		sum_down_double += 1.0f/i;
	}
	printf("DOUBLE SUM UP = \t%lf\n",sum_up_double);
	printf("DOUBLE SUM DOWN =\t%lf\n",sum_down_double);
	printf("Here the difference is much smaller because doubles are the shit\n");
	
	return 0;
}
