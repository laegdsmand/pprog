#include"equal.h"
#include<math.h>
#include<stdio.h>
#include<assert.h>

int main(){
	printf("Exercise 3 in Epsilon\n");
	printf("The equal method is linked to this file\n");
	// Assert that 1 and 1 are equal
	assert(equal(1.0,1.0,0.1,0.1));
	// Assert that 1 and 1.1 are equal with tau = 0.2 and epsilon = 0.2
	assert(equal(1.0,1.1,0.2,0.2));
	// Assert that 1 and 2 are not equal with tau = 0.5 and epsilon = 0.2
	assert(!equal(1.0,2.0,0.5,0.2));
	printf("Looks like it works :)\n");

}

