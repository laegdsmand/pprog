#include <math.h>
/* Returns 1 if the absolute difference b/w a and b is less than tau
or if the the relative difference |a-b|/(|a|+|b|) is less than epsilon/2*/
int equal(double a, double b, double tau, double epsilon){
	double absoluteDiff = fabs(a-b);
	double relativeDiff = absoluteDiff / (fabs(a) + fabs(b));
	return absoluteDiff <= tau || relativeDiff <= epsilon/2;
}

