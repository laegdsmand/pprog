#include<stdio.h>
#include"nvector.h"
#include<stdlib.h>
#include<assert.h>

nvector* nvector_alloc(int n){
	nvector* v = malloc(sizeof(nvector));
	(*v).size = n;
	(*v).data = malloc(n*sizeof(double));
	if(v==NULL) fprintf(stderr, "error in nvector_alloc\n");
	return v;
}

void nvector_free(nvector* v){
	free((*v).data);
	free(v);
}

void nvector_set(nvector* v, int i, double value){
	assert(0 <= i && i < (*v).size);
	(*v).data[i] = value;	
}

double nvector_get(nvector* v, int i){
	assert(0 <= i && i < (*v).size);
	return (*v).data[i];	
}

/* vector u and v must have same size!*/
double nvector_dot_product(nvector* u, nvector* v){
	assert((*u).size == (*v).size);
	double dp = 0;
	for(int i = 0; i < (*u).size; i++){
		dp += (*u).data[i] * (*v).data[i];	
	}
	return dp;
}

void nvector_add(nvector* a, nvector* b){
	assert((*a).size == (*b).size);
	for(int i = 0; i < (*a).size; i++){
		(*a).data[i] += (*b).data[i];
	}
}

int nvector_equal(nvector* a, nvector* b){	
	assert((*a).size == (*b).size);
	int equal = 1;
	for(int i = 0; i < (*a).size; i++){
		equal = equal && (*a).data[i]==(*b).data[i];
	}
	return equal;
}

void nvector_print(char* s, nvector* v){
	printf("%s (", s);
	for(int i = 0; i < (*v).size-1; i++){
		printf("%lg,",(*v).data[i]);
	}
	printf("%lg)",(*v).data[(*v).size-1]);
}

