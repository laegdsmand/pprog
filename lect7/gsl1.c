#include<gsl/gsl_sf_airy.h>
#include<stdio.h>
int main(){
	printf("x\tAi\tBi\n");
	for(double x = -10; x < 7.0; x+= 0.05){
		double Ai = gsl_sf_airy_Ai(x, GSL_PREC_DOUBLE);
		double Bi = gsl_sf_airy_Bi(x, GSL_PREC_DOUBLE);
		printf("%lg\t%lg\t%lg\n",x,Ai,Bi);
	}
}
