#include<gsl/gsl_linalg.h>
#include<gsl/gsl_blas.h>
#include<stdio.h>

int main(){
	size_t dimension = 3;
	gsl_vector* b = gsl_vector_alloc(dimension);
	// initialize vextor b
	gsl_vector_set(b,0,6.23);
	gsl_vector_set(b,1,5.37);
	gsl_vector_set(b,2,2.29);
	//initialize matrix A
	gsl_matrix* A = gsl_matrix_alloc(dimension,dimension);	
	gsl_matrix* N = gsl_matrix_alloc(dimension,dimension);
	gsl_matrix_set(A,0,0,6.13);
	gsl_matrix_set(A,0,1,-2.90);
	gsl_matrix_set(A,0,2,5.86);
	gsl_matrix_set(A,1,0,8.08);
	gsl_matrix_set(A,1,1,-6.31);
	gsl_matrix_set(A,1,2,-3.89);
	gsl_matrix_set(A,2,0,-4.36);
	gsl_matrix_set(A,2,1,1.00);
	gsl_matrix_set(A,2,2,0.19);
	gsl_matrix_memcpy(N,A);
	gsl_vector* x = gsl_vector_alloc(dimension);
	int solver_status = gsl_linalg_HH_solve(A,b,x);

	printf("Linalg solver status is %i\n", solver_status);
	printf("The solution to Ax = b is the vector\n");
	gsl_vector_fprintf(stdout, x, "%g");
	gsl_vector* b_result = gsl_vector_alloc(dimension);
	printf("\n and multiplying A and x gives us b = \n");
	gsl_blas_dgemv(CblasNoTrans, 1.0, N, x, 0.0, b_result);
	gsl_vector_fprintf(stdout, b_result, "%g");
	gsl_vector_free(b);
	gsl_vector_free(b_result);
	gsl_vector_free(x);
	gsl_matrix_free(A);
	gsl_matrix_free(N);
	return 0;
}
