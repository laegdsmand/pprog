#include<stdio.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_odeiv2.h>
#include<assert.h>
#include<math.h>
void part1();
void part2();


int main(int argc, char** argv){
	assert(argc == 2);	
	
	if(atoi(argv[1])==1){
		part1();
	}else{
		part2();
	}
	return 0;
}

void part1(){
	int func(double t, const double y[], double dydt[], void *params){
			dydt[0] = y[0]*(1-y[0]);
			return GSL_SUCCESS;
	};

	/*We dont provide a jacobian for the system in this case, the algorithm can
	do without it */
	gsl_odeiv2_system sys = {func, NULL, 1, NULL};
	const gsl_odeiv2_step_type * T = gsl_odeiv2_step_rkf45;	
	gsl_odeiv2_driver* driver = 
		gsl_odeiv2_driver_alloc_y_new(&sys, T, 1e-6, 1e-6, 0.0);
	int N_steps = 200;
	double t_init = 0.0, t = t_init;
	double t_final = 3.0;
	double y[1] = {0.5}; //Set the initial value of y
	for(int i = 1; i <= N_steps; i++){
		double t_i = i * (t_final - t_init)/N_steps;
		int status = gsl_odeiv2_driver_apply(driver, &t, t_i, y);
		if(status != GSL_SUCCESS) {
			fprintf(stderr, "Error in odeiv2_driver_apply");
		}
		printf("%.5e %.5e\n", t, y[0]);
	}
	gsl_odeiv2_driver_free(driver);
}

void part2(){
	fprintf(stderr,"part2\n");	
	int func(double t, const double y[], double dydt[], void *params){
			double epsilon = *(double*) params;
			dydt[0] = y[1];
			dydt[1] = 1 - y[0] + epsilon * y[0] * y[0];
			return GSL_SUCCESS;
	};
	
	/*We dont provide a jacobian for the system in this case, the algorithm can
	do without it */
	double epsilon = 0.0;
	gsl_odeiv2_system sys = {func, NULL, 2, &epsilon};
	const gsl_odeiv2_step_type * T = gsl_odeiv2_step_rkf45;	
	gsl_odeiv2_driver* driver = 
		gsl_odeiv2_driver_alloc_y_new(&sys, T, 1e-6, 1e-6, 0.0);
	int N_steps = 500;
	double t_init = 0.0, t = t_init;
	double t_final = 6.5;
	
	double y[2] = {1.0, 0.0}; //Set the initial value of y
	for(int i = 1; i <= N_steps; i++){
		double t_i = i * (t_final - t_init)/N_steps;
		int status = gsl_odeiv2_driver_apply(driver, &t, t_i, y);
		if(status != GSL_SUCCESS) {
			fprintf(stderr, "Error in odeiv2_driver_apply");
		}
		
		printf("%.5e %.5e\n", t, y[0]);
	}
	gsl_odeiv2_driver_free(driver);
	driver = 
		gsl_odeiv2_driver_alloc_y_new(&sys, T, 1e-6, 1e-6, 0.0);
	printf("\n\n#Newtonian elliptical motion\n");
	t_init = 0.0, t = t_init;
	t_final =50.0;
	y[0] = 1.0; y[1] = -0.5; //Set the initial value of y
	for(int i = 1; i <= N_steps; i++){
		double t_i = i * (t_final - t_init)/N_steps;
		int status = gsl_odeiv2_driver_apply(driver, &t, t_i, y);
		if(status != GSL_SUCCESS) {
			fprintf(stderr, "Error in odeiv2_driver_apply");
		}
			
		printf("%.5e %.5e\n", t, y[0]);
	}
	gsl_odeiv2_driver_free(driver);
	driver = 
		gsl_odeiv2_driver_alloc_y_new(&sys, T, 1e-6, 1e-6, 0.0);
	N_steps = 5000;	
	printf("\n\n#Relativistic precession\n");
	epsilon = 0.01;
	t_init = 0.0, t = t_init;
	t_final =196*M_PI;
	y[0] = 1.0; y[1] = -0.5; //Set the initial value of y
	for(int i = 1; i <= N_steps; i++){
		double t_i = i * (t_final - t_init)/N_steps;
		int status = gsl_odeiv2_driver_apply(driver, &t, t_i, y);
		if(status != GSL_SUCCESS) {
			fprintf(stderr, "Error in odeiv2_driver_apply");
		}
			
		printf("%.5e %.5e\n", t, y[0]);
	}
	gsl_odeiv2_driver_free(driver);

}
