#include <stdio.h>
#include <math.h>
#include <gsl/gsl_integration.h>

double func (double x, void * params) {
	return log(x) / sqrt(x);
} 

int main () {
	int N_intervals = 1000;
	gsl_integration_workspace* w = gsl_integration_workspace_alloc(N_intervals);
	double result, error;
	double expected = -4.0;
	
	gsl_function F;
	F.function = func; // Should automatically pass pointer to func
	F.params = NULL;
	
	gsl_integration_qags(&F, 0, 1, 0.0, 1e-7, 1000, w, &result, &error);
	
	printf("result is \t %.18f\n", result);
	printf("Expected result\t %.18f\n", expected);
	printf("Est error \t %.18f\n", error);
	printf("Actual error \t %.18f\n", result - expected);
	printf("NO intervals\t %zu\n", w->size);
	gsl_integration_workspace_free(w);
	return 0;
}
