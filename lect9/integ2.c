#include <stdio.h>
#include <math.h>
#include <gsl/gsl_integration.h>

double norm_integrand (double x, void * params) {
	double alpha = *(double *) params;
	return exp(-alpha*x*x);	
}

double hamiltonian_integrand (double x, void * params){
	double alpha = *(double *) params;
	return 1/2.0 * (-alpha*alpha*x*x + alpha + x*x)*exp(-alpha*x*x);
}

int main () {
	int N_intervals = 1000;
	gsl_integration_workspace* w1 = gsl_integration_workspace_alloc(N_intervals);
	gsl_integration_workspace* w2 = gsl_integration_workspace_alloc(N_intervals);

	double result_norm, error;
	double expected = sqrt(M_PI);
	double alpha = 1.0;
	
	gsl_function F_norm;
	F_norm.function = norm_integrand; // Should automatically pass pointer to func
	F_norm.params = &alpha;
	
	gsl_integration_qagiu(&F_norm, 0, 0.0, 1e-7, 1000, w1, &result_norm, &error);
	result_norm *= 2; // use symmetry of wavefunction	
	printf("The norm integral gives us \t  %.10f\n", result_norm);
	printf("Expected value of it was  \t  %.10f\n", expected);
	printf("The error of calculation  \t  %.8e\n\n", result_norm-expected);

	
	double result_hamiltonian;
	expected = sqrt(M_PI)/2;
	
	gsl_function F_hamiltonian;
	F_hamiltonian.function = hamiltonian_integrand;
	F_hamiltonian.params = &alpha;
	
	gsl_integration_qagiu(
		&F_hamiltonian, 0, 0.0, 1e-7, 1000, w2, &result_hamiltonian, &error);
	result_hamiltonian *= 2; // use symmetry of wavefunction	
	printf("The hamiltonian integral  \t  %.10f\n", result_hamiltonian);
	printf("Expected value of it was  \t  %.10f\n", expected);
	printf("The error of calculation  \t  %.8e\n\n", result_hamiltonian-expected);
	
	printf("E(alpha = 1.0) is then    \t  %.10f\n", result_hamiltonian / result_norm);
	printf("Expected value of it was  \t  0.5\n");

	gsl_integration_workspace_free(w1);
	gsl_integration_workspace_free(w2);
}
