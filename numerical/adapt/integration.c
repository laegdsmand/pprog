#include<math.h>
#include<stdio.h>
#include<assert.h>
#include"integration.h"

double adapt24(
	double f(double x, size_t* n_calls),
	double a,
	double b,
	double acc,
	double eps,
	double fx2,
	double fx3,
	double* err,
	size_t n_recursions,
	size_t* n_calls
){
	double fx1 = f(a+(b-a)/6, n_calls); /* Calculate the function values not reused */
	double fx4 = f(a+(b-a)*5/6, n_calls);
	double Q = (2*fx1+fx2+fx3+2*fx4)/6*(b-a);	//Trapezium rule
	double q = (fx1+fx2+fx3+fx4)/4*(b-a);		//rectangle rule
	double tol = acc+eps*fabs(Q);			//Error tolerance
	*err = fabs(Q-q);
					//Error estimate
	if((*err < tol) | (n_recursions>1e4)){
		return Q; 				//Error OK
	}else{						
		n_recursions++;				//Divide into 2 subintervals, recursion
		double Q1 = adapt24(f,a,(a+b)/2,acc/sqrt(2),eps,fx1,fx2,err,n_recursions,n_calls);
		double Q2 = adapt24(f,(a+b)/2,b,acc/sqrt(2),eps,fx3,fx4,err,n_recursions,n_calls);
		return Q1+Q2;
	}
}


double adapt24_start(
	double f(double x),
	double a,
	double b,
	double acc,
	double eps,
	double* err,
	size_t* n_calls
){	
	double f_adapt(double x, size_t* n_calls){
		(*n_calls)++;
		return f(x);
	}
	//Calculate function at #2 and #3 point
	double fx2 = f_adapt(a+(b-a)*2/6,n_calls);
	double fx3 = f_adapt(a+(b-a)*4/6,n_calls);
	return adapt24(f_adapt, a, b, acc, eps, fx2, fx3, err, 0, n_calls);
}
double integrate(
	double f(double x),
	double a,
	double b,
	double acc,
	double eps,
	double* err,
	size_t* n_calls
){
	if((isinf(b)) & (~isinf(a))){
		double f_adapt(double t){
			return f(a+(1-t)/t)/(t*t);
		}
		return adapt24_start(f_adapt,0,1,acc,eps,err,n_calls);
	}else if(isinf(b) & isinf(a)){
		double f_adapt(double t){
			return (f((1-t)/t)+f(-(1-t)/t))/(t*t);
		}
		return adapt24_start(f_adapt,0,1,acc,eps,err,n_calls);
	}else if((~isinf(b)) & isinf(a)){
		double f_adapt(double t){
			return (f(b-(1-t)/t))/(t*t);
		}
		return adapt24_start(f_adapt,0,1,acc,eps,err,n_calls);
	}else{	
		return adapt24_start(f,a,b,acc,eps,err,n_calls);
	}
}

