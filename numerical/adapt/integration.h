#ifndef INTEGRATION
#define INTEGRATION

#include<stddef.h>

double adapt24(
	double f(double x, size_t* n_calls),
	double a,
	double b,
	double acc,
	double eps,
	double fx2,
	double fx3,
	double* err,
	size_t n_recursions,
	size_t* n_calls
);
double integrate(
	double f(double),
	double a,
	double b,
	double acc,
	double eps,
	double* err,
	size_t* n_calls
);

#endif
