#include"integration.h"
#include<math.h>
#include<stdio.h>
#include<assert.h>

double integrand1(double x){
	return sin(x);
}

double integrand2(double x){
	assert(x >= 0);
	return sqrt(x);
}

double integrand3(double x){
	assert(x != 1);
	return 2/sqrt(1-x*x);
}

double integrand4(double x){
	return 4*sqrt(1-(1-x)*(1-x));
}

double integrand5(double x){
	return 1/(x*x);
}

double integrand6(double x){
	return x*x*x*x*exp(x);
}

void test_integrals(){
	size_t n_calls = 0;
	double err;
	double I = integrate(integrand1,0,3.14159,1e-6,1e-6,&err,&n_calls);
	printf("---Testing adaptive integration --- \n");
	printf("First target is sin(x) from 0 to PI \n");
	printf("\t I = %g, should be 2\n", I);
	printf("Second target is sqrt(x) from 0 to 1\n");
	I = integrate(integrand2,0,1,1e-6,1e-6,&err,&n_calls);
	printf("\t I = %g, should be 2/3\n", I);
	printf("Third target is 2/sqrt(1-x^2) from 0 to 1\n");
	I = integrate(integrand3,0,1-1e-9,0,1e-8,&err,&n_calls);
	printf("\t I = %g, should be PI \n", I);
	printf("Fourth target is 4/sqrt(1-(1-x)^2) from 0 to 1\n");
	I = integrate(integrand4,0,1,1e-15,0,&err,&n_calls);
	printf("\t I = %.15f, should be PI, took %lu calls\n", I, n_calls);
	printf("\n--- Testing integration of improper integrals ---\n");
	printf("Fifth target is 1/x^2 from 1 to infty\n");
	I = integrate(integrand5,1,INFINITY,1e-9,0,&err,&n_calls);
	printf("\t I = %f, should be 1, took %lu calls\n", I, n_calls);
	printf("Sixth target is x^4 * exp(x) from -infty to 0\n");
	I = integrate(integrand6,-INFINITY,0,1e-6,0,&err,&n_calls);
	printf("\t I = %f, should be 4!, took %lu calls\n", I, n_calls);
}

int main(int argc, char** argv){
	test_integrals();
	return 0;
}


