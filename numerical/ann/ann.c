#include"ann.h"
#include<assert.h>
#include<gsl/gsl_multimin.h>
#include<stdio.h>

ann* ann_alloc(
	int n_neurons,		
 	int n_vars,
	double f(double)
){
	ann* network = (ann*) malloc(sizeof(ann));
	network->n_neurons = n_neurons;
	network->n_vars = n_vars;
	network->n_params = n_vars*2+1;
	network->data = gsl_vector_alloc((network->n_params)*n_neurons);
	network->f = f;
	gsl_vector_set_all(network->data,2.0);
	return network;
}

void ann_free(ann* network){
	gsl_vector_free(network->data);
	free(network);
}

int ann_get_a(ann* network, int neuron, int var){
	assert(var < network->n_vars);
	assert(neuron < network->n_neurons);
	
	return (network->n_params)*neuron + var;
}

int ann_get_b(ann* network, int neuron, int var){
	assert(var < network->n_vars);
	assert(neuron < network->n_neurons);
	return (network->n_params)*neuron + network->n_vars + var;
}

int ann_get_w(ann* network, int neuron){
	assert(neuron < network->n_neurons);
	return (network->n_params)*neuron + (network->n_vars)*2;
}

//Returns the approximated function value f(x) based on parameters
double ann_feed_forward(ann* network, gsl_vector* x){	
	assert(x->size == network->n_vars);
	double sum=0, a, b, w;
	for(int neuron = 0; neuron < network->n_neurons; neuron++){
		double argument = 0;
		for(int var = 0; var < network->n_vars; var++){
			a = gsl_vector_get(network->data,ann_get_a(network,neuron,var));	
			b = gsl_vector_get(network->data,ann_get_b(network,neuron,var));
			assert(b!=0);	
			argument += (gsl_vector_get(x,var)-a)/b;
		}
		w = gsl_vector_get(network->data,ann_get_w(network,neuron));
		sum += (network->f(argument))*w;
	}
	return sum;
}


void multimin_driver(double f(const gsl_vector* x,void* params), gsl_vector* x){
  const gsl_multimin_fminimizer_type *T = gsl_multimin_fminimizer_nmsimplex2rand;
  gsl_multimin_fminimizer* s;
  gsl_vector* ss;
  size_t iter = 0;
  int dim = x->size;
  int status;
  double size;

  gsl_multimin_function mm_fun;
  mm_fun.f = f;
  mm_fun.n = x->size;
	

  /* Set initial step sizes to 1 */
  ss = gsl_vector_alloc (dim);
  gsl_vector_set_all (ss, 1.0);

  s = gsl_multimin_fminimizer_alloc (T, dim);
  gsl_multimin_fminimizer_set (s, &mm_fun, x, ss);

  do
    {
      iter++;
      status = gsl_multimin_fminimizer_iterate(s);

      if (status)
        break;

      size = gsl_multimin_fminimizer_size (s);
      status = gsl_multimin_test_size (size, 1e-2);

      /*if (status == GSL_SUCCESS)
        {
          printf ("converged to minimum at\n");
        }
	printf ("%5d %10.3e %10.3e f() = %7.3f size = %.3f\n",
              iter,
              gsl_vector_get (s->x, 0),
              gsl_vector_get (s->x, 1),
              s->fval, size); */
    }
  while (status == GSL_CONTINUE && iter < 50000);

  gsl_vector_memcpy(x,s->x);
  gsl_vector_free(ss);
  gsl_multimin_fminimizer_free (s);
}

void ann_predict(ann* network, gsl_matrix* xlist, gsl_vector* ypred){
	for(int i = 0; i<ypred->size; i++){
		gsl_vector_view x = gsl_matrix_row(xlist,i);
		double ffy = ann_feed_forward(network,&x.vector);
		gsl_vector_set(ypred,i,ffy);
	}
}

void ann_train(ann* network, gsl_matrix* xlist, gsl_vector* ylist){
	double deviation(const gsl_vector* data, void* meh){
		gsl_vector_memcpy(network->data,data);
		double error = 0;
		for(int i = 0; i < ylist->size; i++){
			gsl_vector_view x = gsl_matrix_row(xlist,i);
			double ffy = ann_feed_forward(network,&(x.vector));
			error += pow(ffy-gsl_vector_get(ylist,i),2);
		}
		return error;
	}	
	gsl_vector* xstart = gsl_vector_alloc(network->data->size);
	gsl_vector_set_all(xstart,2.0);
	multimin_driver(deviation,xstart);
	gsl_vector_free(xstart);
}


