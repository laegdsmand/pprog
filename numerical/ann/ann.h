#ifndef ANN
#define ANN
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>

/*Neural network for function approximation */
typedef struct {
	int n_neurons;		//Neurons in each layer
	int n_vars; 		//Number of hidden layers
	int n_params;		//Number of parameters for each neuron
	double (*f)(double); 	//Activation function
	gsl_vector* data;	//Adjustable parameters
} ann;

ann* ann_alloc(
	int n_neurons,		
 	int n_vars,
	double f(double)
);

double ann_feed_forward(ann* network, gsl_vector* x);

void ann_predict(ann* network, gsl_matrix* xlist, gsl_vector* ypred);
//Gets index of parameter
int ann_get_a(ann* network, int neuron, int var);
int ann_get_b(ann* network, int neuron, int var);
int ann_get_w(ann* network, int neuron);
void ann_free(ann* network);

void ann_train(ann* network, gsl_matrix* xlist, gsl_vector* ylist);
#endif
