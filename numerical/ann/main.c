#include "ann.h"
#include<assert.h>
#include<stdio.h>
#include<math.h>

void test_ann(){
	double f(double x){
		return 0;
	}
	ann* network = ann_alloc(4,2,f);
	printf("Alloced an network with %li parameters\n", network->data->size);
	assert(ann_get_a(network,1,1)==5+1);

	gsl_vector* x = gsl_vector_alloc(2);
	gsl_vector_set(x,0,1);
	gsl_vector_set(x,1,1);
	assert(ann_feed_forward(network,x)==0);
	ann_free(network);
}

void test_ann_training_1D(){
	double f(double x){
		return x*exp(-x*x);
	}
	ann* network = ann_alloc(8,1,f);	
	gsl_matrix* xlist = gsl_matrix_alloc(20,1);
	gsl_vector* ylist = gsl_vector_alloc(20);
	for(int i = 0; i < xlist->size1; i++){
		double xi = 6.0/20*(i+1);
		gsl_matrix_set(xlist,i,0,xi);
		gsl_vector_set(ylist,i,9-pow(xi-2.0,3));
	}
	//printf("Feed forward 1 = %g\n", ann_feed_forward(network,&ylist.vector));	
	ann_train(network,xlist, ylist);	
	gsl_matrix* xpred = gsl_matrix_alloc(60,1);
	gsl_vector* ypred = gsl_vector_alloc(60);
	for(int i = 0; i < 60; i++){
		gsl_matrix_set(xpred,i,0,6.0/60.0*(i+1));
	}
	ann_predict(network, xpred, ypred);
	for(int i = 0; i<20; i++){
		printf("%g\t%g\n",
			gsl_matrix_get(xlist,i,0),
			gsl_vector_get(ylist,i)	
			);
	}
	printf("\n\n");
	for(int i = 0; i<60;i++){
		printf("%g\t%g\n", gsl_matrix_get(xpred,i,0), gsl_vector_get(ypred,i));
	}
	ann_free(network);
	gsl_vector_free(ypred);
	gsl_matrix_free(xpred);
}

void test_ann_training_2D(){
	double f(double x){
		return x*exp(-x*x);
	}
	ann* network = ann_alloc(22,2,f);	
	int xmax = 10;
	gsl_matrix* xlist = gsl_matrix_alloc(xmax*xmax,2);
	gsl_vector* ylist = gsl_vector_alloc(xlist->size1);
	for(int i = 0; i < xmax; i++){
		for(int j = 0; j < xmax; j++){
			gsl_matrix_set(xlist,i*xmax+j,0,i);
			gsl_matrix_set(xlist,i*xmax+j,1,j);
			gsl_vector_set(ylist,i*xmax+j,pow(4-i,3)+20*j);
		}
	}
	//printf("Feed forward 1 = %g\n", ann_feed_forward(network,&ylist.vector));	
	ann_train(network,xlist,ylist);	
	
	gsl_vector* ypred = gsl_vector_alloc(xlist->size1);
	ann_predict(network, xlist, ypred);
	for(int i = 0; i<xlist->size1; i++){
		printf("%g\t%g\t%g\n",
			gsl_matrix_get(xlist,i,0),
			gsl_matrix_get(xlist,i,1),
			gsl_vector_get(ylist,i)	
			);
	}
	printf("\n\n");
	for(int i = 0; i<xlist->size1;i++){
		printf("%g\t%g\t%g\n",
		 gsl_matrix_get(xlist,i,0),gsl_matrix_get(xlist,i,1), gsl_vector_get(ypred,i));
	}
	ann_free(network);
	gsl_vector_free(ypred);	
}
int main(int argc, char** argv){
	//test_ann();
	if(argc==2){
		if(atoi(argv[1])==1){
			test_ann_training_1D();
		}else if(atoi(argv[1])==2){
			test_ann_training_2D();
		}
	}
	return 0;
}
