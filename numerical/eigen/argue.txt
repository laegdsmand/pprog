a.
Eliminate the off-diagonal elements in the first row only (by running the sweeps not over the whole matrix, but only over the first row until the off-diagonal elements of the first row are all zeroed). Argue, that the corresponding diagonal element is the lowest eigenvalue.
We will look as an example on the following diagonal matrix with scrambled eigenvalues 
    [1  0  0]
A = [0 -2  0]
    [0  0  5]

Eliminating off-diagonal elements for first row is done by applying the Jacobi rotations
A' = J(1,2)^T * A * J(1,2)    ,(phi = pi/2)

    [-2  0  0]
A'= [ 0  1  0]
    [ 0  0  5]

and
A'' = J(1,3)^T * A * J(1,3)   ,(phi = 0)

     [-2  0  0]
A''= [ 0  1  0]
     [ 0  0  5]
We see that the eigenvalues are switched arond so first element is eigenval -2

b. 
if needed, eliminate the off-diagonal elements in the second row. Argue, that the eliminated elements in the first row will not be affected and can therefore be omitted from the elimination loop. Argue that the corresponding diagonal element is the second lowest eigenvalue.
We see that by applying J(2,3) we get the same matrix with eigenvalue 1 in 2nd diagonal element.
---

2.
To change the algorithm to sort highest eigenvalues first we change the phi formula to
	phi = - 0.5 * atan2(2*a_pq, a_pp-a_qq);
