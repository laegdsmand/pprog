#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include"aux.h"

void jacobi_get_D(gsl_matrix* D, gsl_vector* e){
	assert(D->size1 == D->size2);
	assert(D->size1 == e->size);
	gsl_matrix_set_zero(D);
	for(int i = 0; i < e->size; i++){
		gsl_matrix_set(D,i,i, gsl_vector_get(e,i));
	}
}

gsl_matrix* alloc_random_sym_matrix(int n){
	double max = RAND_MAX;
	gsl_matrix* A = gsl_matrix_alloc(n,n);
	for(int i = 0; i < n; i++)for(int j = i; j < n; j++){
		double value = rand()/max;
		gsl_matrix_set(A,i,j, value);
		gsl_matrix_set(A,j,i, value);
	}
	return A;
}
void print_matrix_pretty(gsl_matrix *A){
	for(int i = 0;i<A->size1; i++){
		printf("[");
		for(int j = 0;j<A->size2;j++){
			printf("%.3e  ",gsl_matrix_get(A,i,j));
		}
		printf("]\n");
	}
}


int jacobi_rotation(gsl_matrix* A, gsl_vector* e, gsl_matrix* V, int p, int q){	
	int changed = 0, n = A->size1;
	double a_pp = gsl_vector_get(e,p);//p'th element of diagonal
	double a_qq = gsl_vector_get(e,q);//q'th element of diagonal
	double a_pq = gsl_matrix_get(A,p,q);//(A)_pq
	double phi = - 0.5 * atan2(2*a_pq, a_pp-a_qq);
	double c = cos(phi), s = sin(phi);
	//Calculate new elements according to rules
	double a1_pp = c*c*a_pp - 2*s*c*a_pq + s*s*a_qq;
	double a1_qq = s*s*a_pp + 2*s*c*a_pq + c*c*a_qq;
	if(a1_pp!=a_pp || a1_qq!=a_qq){
		changed = 1;
		gsl_vector_set(e,p,a1_pp);
		gsl_vector_set(e,q,a1_qq);
		gsl_matrix_set(A,p,q,0.0);
		for(int i = 0; i<p; i++){
			double a_ip = gsl_matrix_get(A,i,p);
			double a_iq = gsl_matrix_get(A,i,q);
			gsl_matrix_set(A,i,p, c*a_ip - s*a_iq);
			gsl_matrix_set(A,i,q, c*a_iq + s*a_ip);
		}
		for(int i=p+1; i<q; i++){
			double a_pi = gsl_matrix_get(A,p,i);
			double a_iq = gsl_matrix_get(A,i,q);
			gsl_matrix_set(A,p,i, c*a_pi - s*a_iq);
			gsl_matrix_set(A,i,q, c*a_iq + s*a_pi);
		}
		for(int i=q+1; i<n; i++){
			double a_pi = gsl_matrix_get(A,p,i);
			double a_qi = gsl_matrix_get(A,q,i);
			gsl_matrix_set(A,p,i, c*a_pi - s*a_qi);
			gsl_matrix_set(A,q,i, c*a_qi + s*a_pi);
		}
		for(int i=0;i<n;i++){
			double v_ip=gsl_matrix_get(V,i,p);
			double v_iq=gsl_matrix_get(V,i,q);	
			gsl_matrix_set(V,i,p,c*v_ip-s*v_iq);
			gsl_matrix_set(V,i,q,c*v_iq+s*v_ip); 
		}
	}
	return changed;
}
