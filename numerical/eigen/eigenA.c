#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include"aux.h"



int jacobi_sweep(gsl_matrix* A, gsl_vector* e, gsl_matrix* V){
	int changed,p,q,n=A->size1;
	//loop goes through every element below diagonal
	for(p=0; p<n; p++)for(q=p+1; q<n; q++){
		changed = jacobi_rotation(A,e,V,p,q);
	}
	return changed;
}


//Jacobi diagonalization using cyclic sweeps of real symmetric matrix A
int jacobi(gsl_matrix* A, gsl_vector* e, gsl_matrix* V){
	int changed, sweeps=0, n=A->size1;
	gsl_matrix_set_identity(V);
	//Set elements of e to diagonal of A
	for(int i = 0; i < n; i++){
		gsl_vector_set(e,i,gsl_matrix_get(A,i,i));
	}
	do{
		changed = jacobi_sweep(A,e,V);
		sweeps++;
		
	}while(changed != 0);
	return sweeps;
}


void test_jacobi(){
	int n = 4;	
	gsl_matrix* A = alloc_random_sym_matrix(n);
	gsl_vector* e = gsl_vector_alloc(n);
	gsl_matrix* A1 = gsl_matrix_alloc(n,n);
	gsl_matrix_memcpy(A1,A);
	gsl_matrix* V = gsl_matrix_alloc(n,n);
	gsl_matrix* D = gsl_matrix_alloc(n,n);
	printf("A = \n");
	print_matrix_pretty(A);
	jacobi(A1,e,V);
	printf("Applying Jacobi diagonalization\n A'  = \n");
	print_matrix_pretty(A1);
	printf("V = \n");
	print_matrix_pretty(V);
	printf("e = \n");
	gsl_vector_fprintf(stdout,e,"%.3e");
	jacobi_get_D(D,e);
	printf("D = \n");
	print_matrix_pretty(D);
	printf("V^T *  V = \n");
	gsl_matrix* product = gsl_matrix_alloc(n,n);
	gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,V,V,0.0,product);	
	print_matrix_pretty(product);
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,A,V,0.0,product);	
	gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,V,product,0.0,D);	
	printf("V^T * A * V = D =  \n");
	print_matrix_pretty(D);
	gsl_matrix_free(A);
	gsl_matrix_free(A1);
	gsl_matrix_free(V);
	gsl_matrix_free(D);
	gsl_matrix_free(product);
	gsl_vector_free(e);
}

void test_time(int n){
	gsl_vector* e = gsl_vector_alloc(n);
	gsl_matrix* A = alloc_random_sym_matrix(n);
	gsl_matrix* V = gsl_matrix_alloc(n,n);
	jacobi(A,e,V);
	gsl_matrix_free(A);
	gsl_matrix_free(V);
	gsl_vector_free(e);
}

int main(int argc, char** argv){
	if(argc > 1){
		int n = atoi(argv[1]);
		test_time(n);
	}else{
		test_jacobi();
	}
	return 0;
}

