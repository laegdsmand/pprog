#ifndef EIGENA
#define EIGENA
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
	
void print_matrix_pretty(gsl_matrix *A);
int jacobi_rotation(gsl_matrix* A, gsl_vector* e, gsl_matrix* V, int p, int q);	
gsl_matrix* alloc_random_sym_matrix(int n);
void jacobi_get_D(gsl_matrix* D, gsl_vector* e);
#endif
