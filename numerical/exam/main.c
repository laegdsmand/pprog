#include"ode_ann.h"
#include<stdio.h>
#include<math.h>

/*Activation function and its derivative */
double gaussian_wavelet(double x){
	return x*exp(-x*x);
}

double d_gaussian_wavelet(double x){
	return exp(-x*x) - 2*x*x*exp(-x*x);
}

/* The ODE's to solve */
double ode_logistic(double x, double y){
	return y*(1-y);
}

double ode_gaussian(double x, double y){
	return -x*y;
}

double integrand(double x, double y){
	return 2*sqrt(1-x*x); 
}

/* Plots the approximation for logistic function */
void test_ode_logistic(){
	ode_problem problem;
	problem.f = &ode_logistic;
	problem.x0 = 0;
	problem.y0 = 0.5;
	problem.a = -5.0;
	problem.b = 5.0;
	problem.N = 100;	
	ode_ann* nw = ode_ann_alloc(9, &gaussian_wavelet, &d_gaussian_wavelet);
	ode_ann_train(nw,&problem);
	double F,dF,x_i,y_i;
	for(int i = 0; i < problem.N; i++){
		x_i = problem.a + i*(problem.b-problem.a)/(problem.N-1.0);
		y_i = exp(x_i)/(exp(x_i)+1);
		ode_ann_feed_forward(nw, x_i, &F, &dF);
		printf("%g\t%g\t%g\t%g\n", x_i, F, dF, y_i);
	}		
	ode_ann_free(nw);
}

/*Plots the approximation for ODE gaussian function */
void test_ode_gaussian(){
	ode_problem problem;
	problem.f = &ode_gaussian;
	problem.x0 = 0;
	problem.y0 = 1.0;
	problem.a = -5.0;
	problem.b = 5.0;
	problem.N = 100;	
	ode_ann* nw = ode_ann_alloc(5, &gaussian_wavelet, &d_gaussian_wavelet);
	ode_ann_train(nw,&problem);
	double F,dF,x_i,y_i;
	for(int i = 0; i < problem.N; i++){
		x_i = problem.a + i*(problem.b-problem.a)/(problem.N-1.0);
		y_i = exp(-x_i*x_i/2);
		ode_ann_feed_forward(nw, x_i, &F, &dF);
		printf("%g\t%g\t%g\t%g\n", x_i, F, dF, y_i);
	}		
	ode_ann_free(nw);
}
/*Makes out.txt and integrates 2*sqrt(1-x^2) from -1 to 1 */
void test_ode_ann(){
	printf("Exam project in Numerical Methods by Peter Lægdsmand, 201505904\n");
	printf("Artificial neural network for solving ODE\n\n");	
	printf("--- TESTING THE ANN ODE SOLVER ---\n");
	printf("Approximation of the solution to the logistic function ODE has been done\n\
		with 9 neurons and plotted in logistic.svg\n\
		Approximation of the solution to the Gaussian function ODE has been done\n\
		with 5 neurons and plotted in gaussian.svg\n");
	ode_problem problem;
	problem.f = &integrand;
	problem.x0 = -1;
	problem.y0 = 0;
	problem.a = -1.0;
	problem.b = 1.0;
	problem.N = 100;	
	ode_ann* nw = ode_ann_alloc(10, &gaussian_wavelet, &d_gaussian_wavelet);
	ode_ann_train(nw,&problem);	
	double F,dF;
	ode_ann_feed_forward(nw,problem.b,&F,&dF);
	printf("--- Testing integration using ANN ---\n\
		The definite integral of 2*sqrt(1-x^2) from -1 to 1 = %g\n is approximated as %g\n",
		 3.14159265358979,F);
	printf("Using 10 neurons\n");
	ode_ann_free(nw);	
	

	
}

int main(int argc, char** argv){
	if(argc<2){
		test_ode_ann();	
		return 0;
	}
	if(atoi(argv[1])==1) test_ode_logistic();
	if(atoi(argv[1])==2) test_ode_gaussian();
	return 0;
}
