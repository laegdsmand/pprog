#include"ode_ann.h"
#include<assert.h>
#include<gsl/gsl_multimin.h>

/* Functions for allocating and deallocating the memory used for the ANN */
ode_ann* ode_ann_alloc(
	int n_neurons,		
	double f(double),
	double df(double)
){
	ode_ann* network = (ode_ann*) malloc(sizeof(ode_ann));
	network->n_neurons = n_neurons;
	network->n_params = 3;
	network->data = gsl_vector_alloc((network->n_params)*n_neurons);
	network->f = f;
	network->df = df;
	gsl_vector_set_all(network->data,1.0);
	return network;
}


void ode_ann_free(ode_ann* network){
	gsl_vector_free(network->data);
	free(network);
}

/*Gets the index of parameters a b and w for a neuron */
int ode_ann_get_a(ode_ann* network, int neuron){
	assert(neuron < network->n_neurons);
	return (network->n_params)*neuron;
}

int ode_ann_get_b(ode_ann* network, int neuron){
	
	assert(neuron < network->n_neurons);
	return (network->n_params)*neuron + 1;
}

int ode_ann_get_w(ode_ann* network, int neuron){
	assert(neuron < network->n_neurons);
	return (network->n_params)*neuron + 2;
}

/* Makes prediction of the value of F and dF/dx for a given x */
void ode_ann_feed_forward(ode_ann* network, double x, double *F, double *dF){		
	double a, b, w;
	*F = 0;
	*dF = 0;
	for(int neuron = 0; neuron < network->n_neurons; neuron++){
		a = gsl_vector_get(network->data,ode_ann_get_a(network,neuron));
		b = gsl_vector_get(network->data,ode_ann_get_b(network,neuron)); 
		double argument = (x-a)/b;
		w = gsl_vector_get(network->data,ode_ann_get_w(network,neuron));
		*F += (network->f(argument))*w;
		*dF += (network->df(argument))*w/b;
	}
}

/* Drives the minimization for a multimin algorithm. Here the Nelder-Mead Simplex
Algorithm has been chosen to find the minimum */
void multimin_driver(double f(const gsl_vector* x,void* params), gsl_vector* x){
  const gsl_multimin_fminimizer_type *T = gsl_multimin_fminimizer_nmsimplex2rand;
  gsl_multimin_fminimizer* minimizer;
  gsl_vector* ss;
  size_t iteration = 0; 
  int status;
  double size, eps = 1e-3;

  gsl_multimin_function mm_fun;
  mm_fun.f = f;
  mm_fun.n = x->size;
	

  /* Set initial step size*/
  ss = gsl_vector_alloc (mm_fun.n);
  gsl_vector_set_all (ss, 1e-1);

  minimizer = gsl_multimin_fminimizer_alloc (T, mm_fun.n);
  gsl_multimin_fminimizer_set (minimizer, &mm_fun, x, ss);

  do
    {
      iteration++;
      status = gsl_multimin_fminimizer_iterate(minimizer);

      if (status) //Estimate cannot imrove
        break;

      size = gsl_multimin_fminimizer_size (minimizer);
      status = gsl_multimin_test_size (size, eps);

    }
  while (status == GSL_CONTINUE && iteration < 50000);

  gsl_vector_memcpy(x,minimizer->x);
  gsl_vector_free(ss);
  gsl_multimin_fminimizer_free(minimizer);
}

/*Minimizes the deviation of the ANN predictions with respect to the parameters of the 
neutrons */
void ode_ann_train(
	ode_ann* nw,
	ode_problem* problem
){
	gsl_vector* x_mesh = gsl_vector_alloc(problem->N);
	for(int i = 0; i < problem->N; i++){
		double x_i = problem->a + i*(problem->b-problem->a)/(problem->N-1.0);
		gsl_vector_set(x_mesh,i,x_i);
	}
	double deviation(const gsl_vector* data ,void* params){
		gsl_vector_memcpy(nw->data, data);
		double error = 0;
		for(int i = 0; i < problem->N; i++){
			double x_i = gsl_vector_get(x_mesh,i);
			double F_i = 0, dF_i = 0;
			ode_ann_feed_forward(nw,x_i,&F_i, &dF_i);
			error += pow(dF_i-problem->f(x_i,F_i),2);
		}
		double F, dF;
		ode_ann_feed_forward(nw, problem->x0, &F, &dF);
		error += problem->N * pow(F-problem->y0,2);
		return error;
	}
	gsl_vector* pstart = gsl_vector_alloc(nw->data->size);
	gsl_vector_set_all(pstart,1.0);
	for(int i = 0; i<nw->n_neurons;i++){
		gsl_vector_set(
			pstart,ode_ann_get_a(nw,i),
			problem->a + i*(problem->b-problem->a)/(nw->n_neurons-1.0)

		);
	}
	multimin_driver(deviation,pstart);	
	gsl_vector_free(pstart);
}
