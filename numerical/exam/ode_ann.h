#ifndef ODEANN
#define ODEANN
#include<gsl/gsl_vector.h>

typedef struct {
	int n_neurons;		//Neurons in each layer	
	int n_params;		//Number of parameters for each neuron
	double (*f)(double); 	//Activation function
	double (*df)(double);	//The derivative of activation functiuon
	gsl_vector* data;	//Adjustable parameters
} ode_ann;

typedef struct {
	double (*f)(double,double);	//Differential equation
	double x0;			//Initial value
	double y0;			
	double a;			//Approximate in interval [a;b]
	double b;
	double N;			//Number of points in mesh
} ode_problem;

ode_ann* ode_ann_alloc(
	int n_neurons,		
	double f(double),
	double df(double)
);
void ode_ann_free(ode_ann* network);
void ode_ann_feed_forward(ode_ann* network, double x, double *F, double *dF);		
void ode_ann_train(ode_ann* network, ode_problem* p);
#endif
