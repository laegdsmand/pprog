Exam project in Numerical Methods by Peter Lægdsmand, 201505904
Artificial neural network for solving ODE

--- TESTING THE ANN ODE SOLVER ---
Approximation of the solution to the logistic function ODE has been done
		with 9 neurons and plotted in logistic.svg
		Approximation of the solution to the Gaussian function ODE has been done
		with 5 neurons and plotted in gaussian.svg
--- Testing integration using ANN ---
		The definite integral of 2*sqrt(1-x^2) from -1 to 1 = 3.14159
 is approximated as 3.13067
Using 10 neurons
