#ifndef GIVENS_QR
#define GIVENS_QR
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>

void print_matrix_pretty(gsl_matrix* A);

void givens_qr_decomp(gsl_matrix* A);

void givens_qr_qtrans_vector(gsl_matrix* QR, gsl_vector* v);

void givens_qr_get_Q(gsl_matrix* QR,gsl_matrix* Q);

void givens_qr_get_R(gsl_matrix* QR, gsl_matrix* R);

#endif
