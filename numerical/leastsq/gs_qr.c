#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include<assert.h>

//Returns the norm of jth column in A
double norm_column(gsl_matrix* A, int j){
	double norm = 0;
	int n = A->size1;
	for(int i = 0; i < n; i++){
		double a_ij = gsl_matrix_get(A,i,j);
		norm += a_ij*a_ij;
	}
	norm = sqrt(norm);
	return norm;
}

//for easy multiplication
gsl_matrix *  matrix_matrix_product(gsl_matrix* A, gsl_matrix* B){
	gsl_matrix* result = gsl_matrix_alloc(A->size1,B->size2);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1, A, B, 0, result);
	return result;
}


void print_matrix_pretty(gsl_matrix *A){
	for(int i = 0;i<A->size1; i++){
		printf("[");
		for(int j = 0;j<A->size2;j++){
			printf("%.3e  ",gsl_matrix_get(A,i,j));
		}
		printf("]\n");
	}
}

//normalizes the 
void normalize_column(gsl_matrix* A, int j){
	double norm = norm_column(A,j);
	for(int i = 0; i < A->size1; i++){
		gsl_matrix_set(A,i,j,
			gsl_matrix_get(A,i,j) / norm );
	}

}




double dot_product(gsl_vector* a, gsl_vector* b){
	double p = 0;
	int dim = a->size;
	for(int i = 0; i < dim; i++ ){
		p += gsl_vector_get(a, i) * gsl_vector_get(b, i);
	}
	return p;
}

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R){
	gsl_matrix_set_zero(R);
	int m = (int) A->size2;
	int n = (int) A->size1;
	//gsl_vector *q_i = gsl_vector_alloc(n);
	for(int i = 0; i < m; i++){
		
		gsl_matrix_set(R,i,i,
			norm_column(A,i));
		normalize_column(A,i);
		for(int j = i+1; j < m; j++){
			gsl_vector_view a_j = gsl_matrix_column(A,j);
			gsl_vector_view a_i = gsl_matrix_column(A,i);
			double dp = dot_product(&a_j.vector,&a_i.vector);	
			gsl_matrix_set(R,i,j,dp);	
			gsl_vector* a_i_scaled = gsl_vector_alloc(n);
			gsl_vector_memcpy(a_i_scaled, &a_i.vector);
			gsl_vector_scale(a_i_scaled, dp);
			gsl_vector_sub(&a_j.vector, a_i_scaled);
			gsl_vector_free(a_i_scaled);	
			
		}
	}
}

/*Solves the system of linear equations R*x = Q^T * b
 or equivalent QR*x = b*/
void qr_gs_solve(const gsl_matrix* Q, const gsl_matrix* R, const gsl_vector* b,
	 gsl_vector* x)
{
	int m = Q->size2;
	// Calculate Q^T * b
	gsl_blas_dgemv(CblasTrans, 1.0, Q, b, 0.0, x);
	for(int i = m-1; i>=0; i--){
		double x_i = gsl_vector_get(x,i);
		for(int j = i+1; j < m; j++){
			x_i -= gsl_matrix_get(R,i,j) * gsl_vector_get(x,j);
		}
		x_i = x_i / gsl_matrix_get(R,i,i);
		gsl_vector_set(x,i,x_i);
	}
}

void qr_gs_inverse(const gsl_matrix* Q, const gsl_matrix* R, gsl_matrix* B){
	int n = Q->size1;
	gsl_vector* e_i = gsl_vector_alloc(n);
	for(int i = 0; i < n; i++){
		gsl_vector_set_basis(e_i,i);
		gsl_vector_view b_i = gsl_matrix_column(B,i);
		qr_gs_solve(Q,R,e_i, &b_i.vector);
	}
}

void qr_gs_backsub(gsl_matrix* R, gsl_vector* x){
	int m = R->size1;
	for(int i = m-1; i>=0; i--){
		double x_i = gsl_vector_get(x,i);
		for(int j = i+1; j < m; j++){
			x_i -= gsl_matrix_get(R,i,j) * gsl_vector_get(x,j);
		}
		x_i = x_i / gsl_matrix_get(R,i,i);
		gsl_vector_set(x,i,x_i);
	}	
}

void qr_gs_inverse_R(gsl_matrix* R,gsl_matrix* R_inv){
	int m = R->size1;
	gsl_vector* x = gsl_vector_alloc(m);
	for(int i = 0; i<m; i++){
		gsl_vector_set_basis(x,i);
		qr_gs_backsub(R,x);
		for(int j = 0; j<m; j++){
			gsl_matrix_set(R_inv,j,i, gsl_vector_get(x,j));
		}
	}
	gsl_vector_free(x);
}

