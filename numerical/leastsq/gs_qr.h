#ifndef GS_QR
#define GS_QR
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>


void print_matrix_pretty(gsl_matrix* A);

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R);


void qr_gs_solve(const gsl_matrix* Q, const gsl_matrix* R, const gsl_vector* b,
	 gsl_vector* x);

void qr_gs_inverse(const gsl_matrix* Q, const gsl_matrix* R, gsl_matrix* B);

void qr_gs_inverse_R(gsl_matrix* R, gsl_matrix* R_inv);
#endif
