#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include<assert.h>
#include"gs_qr.h"

/*typedef struct {	
	(*fs[])(double x);
	 int m;
	 gsl_vector *x;
	 gsl_vector *y;
	 gsl_vector *dy;
	 gsl_matrix *S;
}o_leastsq
*/
void o_leastsq_fit(
	double (*fs[])(double x), int m, gsl_vector *x, gsl_vector *y, gsl_vector *dy,
	gsl_vector *c, gsl_matrix *S)
{
	int n = x->size;
	gsl_matrix* A = gsl_matrix_alloc(n,m);
	gsl_vector* b = gsl_vector_alloc(n);
	gsl_matrix* R = gsl_matrix_alloc(m,m);
	gsl_matrix* R_inv = gsl_matrix_alloc(m,m);
	
	for(int i=0; i<n; i++){
		gsl_vector_set(b,i, 
			gsl_vector_get(y,i)/gsl_vector_get(dy,i)
		);
		for(int k = 0; k<m; k++){
			gsl_matrix_set(A,i,k,
				fs[k](gsl_vector_get(x,i)) / gsl_vector_get(dy,i) 
			);
		}
		
	}
	qr_gs_decomp(A,R);
	qr_gs_solve(A,R,b,c);
	qr_gs_inverse_R(R,R_inv);
	

	//calculates the covariance matrix S	
	gsl_blas_dgemm(CblasNoTrans,CblasTrans, 1.0, R_inv,R_inv,0.0,S);
	gsl_vector_free(b);
	gsl_matrix_free(A);
}

void plot_fit(double* x_range,int n_points,double (*fs[])(double x),int m, gsl_vector *c){
	double x,y;
	for(int i = 0; i<n_points; i++){
		x = x_range[0] + i*(x_range[1]-x_range[0])/n_points;
		y = 0;
		for(int k = 0; k<m; k++){
			y += gsl_vector_get(c,k) * fs[k](x);	
		}
		printf("%g\t%g\n",x,y);
	}
	printf("\n\n");
}

void get_delta_c(gsl_vector* delta_c, gsl_matrix* S){
	for(int i =0; i<S->size1; i++){
		gsl_vector_set(delta_c, i,
			sqrt(gsl_matrix_get(S,i,i))
		);
	}
}

double fun_1(double x){return 1;}
double fun_x(double x){return x;}

void test_fit_1(){
	
	int m = 3;
	double (*p[m])(double x);
	p[0] = log;
	p[1] = fun_1;
	p[2] = fun_x;
	int n = 9;
	//initialize data
	double x[9] =  {0.1,   1.33,  2.55,  3.78,  5.0,  6.22, 7.45,  8.68,  9.9};
	double y[9] =  {-15.3, 0.32,  2.45,  2.75,  2.27, 1.35, 0.157, -1.23, -2.75};
	double dy[9] = {1.04,  0.594, 0.983, 0.998, 1.11, 0.398,0.535, 0.968, 0.478 };
	for(int i = 0; i<n; i++)printf("%g\t%g\t%g\n",x[i],y[i],dy[i]);
	printf("\n\n");
	//make data into vectors
	gsl_vector_view xv = gsl_vector_view_array(x,n);
	gsl_vector_view yv = gsl_vector_view_array(y,n);
	gsl_vector_view dyv = gsl_vector_view_array(dy,n);
	//result holders
	gsl_matrix* S = gsl_matrix_alloc(m,m);
	gsl_vector* c = gsl_vector_alloc(m);
	gsl_vector* delta_c = gsl_vector_alloc(m);
	o_leastsq_fit(p,m,&xv.vector, &yv.vector, &dyv.vector,c,S);

	
	double x_range[2] = {x[0],x[n-1]};

	//plotting the fit + predint
	get_delta_c(delta_c, S);
	plot_fit(x_range, 100, p, m, c);
	gsl_vector_add(c,delta_c);
	plot_fit(x_range, 100, p, m, c);
	gsl_vector_sub(c,delta_c);
	gsl_vector_sub(c,delta_c);
	plot_fit(x_range, 100, p, m, c);	


	//Free allocated memory	
	gsl_vector_free(c);
	gsl_vector_free(delta_c);
	gsl_matrix_free(S);
}


int main(){
	test_fit_1();
	return 0;
}
