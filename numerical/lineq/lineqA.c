#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include<assert.h>

//Returns the norm of jth column in A
double norm_column(gsl_matrix* A, int j){
	double norm = 0;
	int n = A->size1;
	for(int i = 0; i < n; i++){
		double a_ij = gsl_matrix_get(A,i,j);
		norm += a_ij*a_ij;
	}
	norm = sqrt(norm);
	return norm;
}

//for easy multiplication
gsl_matrix *  matrix_matrix_product(gsl_matrix* A, gsl_matrix* B){
	gsl_matrix* result = gsl_matrix_alloc(A->size1,B->size2);
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1, A, B, 0, result);
	return result;
}


void print_matrix_pretty(gsl_matrix *A){
	for(int i = 0;i<A->size1; i++){
		printf("[");
		for(int j = 0;j<A->size2;j++){
			printf("%.3e  ",gsl_matrix_get(A,i,j));
		}
		printf("]\n");
	}
}

//normalizes the 
void normalize_column(gsl_matrix* A, int j){
	double norm = norm_column(A,j);
	for(int i = 0; i < A->size1; i++){
		gsl_matrix_set(A,i,j,
			gsl_matrix_get(A,i,j) / norm );
	}

}




double dot_product(gsl_vector* a, gsl_vector* b){
	double p = 0;
	int dim = a->size;
	for(int i = 0; i < dim; i++ ){
		p += gsl_vector_get(a, i) * gsl_vector_get(b, i);
	}
	return p;
}

void qr_gs_decomp(gsl_matrix* A, gsl_matrix* R){
	gsl_matrix_set_zero(R);
	int m = (int) A->size2;
	int n = (int) A->size1;
	//gsl_vector *q_i = gsl_vector_alloc(n);
	for(int i = 0; i < m; i++){
		
		gsl_matrix_set(R,i,i,
			norm_column(A,i));
		normalize_column(A,i);
		for(int j = i+1; j < m; j++){
			gsl_vector_view a_j = gsl_matrix_column(A,j);
			gsl_vector_view a_i = gsl_matrix_column(A,i);
			double dp = dot_product(&a_j.vector,&a_i.vector);	
			gsl_matrix_set(R,i,j,dp);	
			gsl_vector* a_i_scaled = gsl_vector_alloc(n);
			gsl_vector_memcpy(a_i_scaled, &a_i.vector);
			gsl_vector_scale(a_i_scaled, dp);
			gsl_vector_sub(&a_j.vector, a_i_scaled);
			gsl_vector_free(a_i_scaled);	
			
		}
	}
}


void qr_gs_solve(const gsl_matrix* Q, const gsl_matrix* R, const gsl_vector* b,
	 gsl_vector* x)
{
	int n = Q->size1;
	// Calculate Q^T * b
	gsl_blas_dgemv(CblasTrans, 1.0, Q, b, 0.0, x);
	for(int i = n-1; i>=0; i--){
		double x_i = gsl_vector_get(x,i);
		for(int j = i+1; j < n; j++){
			x_i -= gsl_matrix_get(R,i,j) * gsl_vector_get(x,j);
		}
		x_i = x_i / gsl_matrix_get(R,i,i);
		gsl_vector_set(x,i,x_i);
	}
}

void qr_gs_inverse(const gsl_matrix* Q, const gsl_matrix* R, gsl_matrix* B){
	int n = Q->size1;
	gsl_vector* e_i = gsl_vector_alloc(n);
	for(int i = 0; i < n; i++){
		gsl_vector_set_basis(e_i,i);
		gsl_vector_view b_i = gsl_matrix_column(B,i);
		qr_gs_solve(Q,R,e_i, &b_i.vector);
	}
}

void test_qr_qs_decomp(){
	printf("\ntesting on tall matrix A\n");
	int n=5,m=3;
	gsl_matrix* A1 = gsl_matrix_alloc(n,m);
	double max = RAND_MAX;
	for(int i=0; i<n; i++){
		for(int j=0; j<m; j++){
			gsl_matrix_set(A1,i,j,rand()/max);
		}
	}
	
	gsl_matrix* R = gsl_matrix_alloc(m,m);
	print_matrix_pretty(A1);
	qr_gs_decomp(A1,R);	
	printf("Q = \n");
	print_matrix_pretty(A1);
	printf("R = \n");	
	print_matrix_pretty(R);
	gsl_matrix* result = matrix_matrix_product(A1,R);
	printf("QR = \n");
	print_matrix_pretty(result);
	gsl_matrix_free(result);
	
	//assert Q^T *Q is 1	
	result = gsl_matrix_alloc(A1->size2,A1->size2);
	gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1, A1,A1, 0, result);
	printf("Q^T * Q = \n");
	print_matrix_pretty(result);
	gsl_matrix_free(result);
	gsl_matrix_free(A1);
	gsl_matrix_free(R);
}

void test_matrix_product(){
	
	double A_vals[9] = {1,1,2,1,2,2,1,3,2};
	double B_vals[9] = {1,1,1,1,1,1,1,1,1};
	gsl_matrix_view A_view = gsl_matrix_view_array(A_vals,3,3);
	gsl_matrix_view B_view = gsl_matrix_view_array(B_vals,3,3);
	gsl_matrix * C = &A_view.matrix, *B = &B_view.matrix;
	gsl_matrix * res = matrix_matrix_product(C,B);
	
	printf("A*B = \n");
	print_matrix_pretty(res);
	gsl_matrix_free(res);
}

void test_qr_gs_inverse(gsl_matrix* A, gsl_matrix* Q, gsl_matrix* R){
	int n = A->size1;
	gsl_matrix* B = gsl_matrix_alloc(n,n);
	qr_gs_inverse(Q,R,B);
	
	printf("We are now computing the inverse B = \n");
	print_matrix_pretty(B);
	printf("We can now check that AB = 1 (identity\n)");
	gsl_matrix* product = matrix_matrix_product(A,B);
	print_matrix_pretty(product);
	gsl_matrix_free(product);
	gsl_matrix_free(B);
}

void test_qr_gs_solve(){
	int n = 3;
	gsl_matrix* A = gsl_matrix_alloc(n,n);
	gsl_matrix* Q = gsl_matrix_alloc(n,n);
	gsl_vector* b = gsl_vector_alloc(n);
	gsl_vector* x = gsl_vector_alloc(n);
	gsl_matrix* R = gsl_matrix_alloc(n,n);
	double  max = RAND_MAX;
	for(int i = 0; i<n; i++){
		for(int j = 0; j < n; j++){
			gsl_matrix_set(A,i,j,rand()/max);
		}
		gsl_vector_set(b,i,rand()/max);
	}
	gsl_matrix_memcpy(Q,A);
	printf("Solving Ax = b\nA = \n");
	print_matrix_pretty(A);
	printf("b = \n");
	gsl_vector_fprintf(stdout,b,"%.3e");
	qr_gs_decomp(Q,R);
	qr_gs_solve(Q,R,b,x);
	printf("x = \n");	
	gsl_vector_fprintf(stdout,x,"%.3e");
	printf("check that Ax = b \n");
	gsl_blas_dgemv(CblasNoTrans, 1.0, A, x, 0.0, b);
	gsl_vector_fprintf(stdout, b, "%.3e");
	test_qr_gs_inverse(A,Q,R);
	gsl_matrix_free(A);
	gsl_matrix_free(R);
	gsl_matrix_free(Q);
	gsl_vector_free(x);
	gsl_vector_free(b);
}


void test(){	
	test_qr_qs_decomp();
	printf("\n");
	test_qr_gs_solve();
	/*
	gsl_matrix* A = gsl_matrix_alloc(2,2);
	gsl_matrix* R = gsl_matrix_alloc(2,2);
	gsl_matrix_set_identity(A);
	gsl_matrix_fprintf(stdout, A, "%g");
	qr_gs_decomp(A,R);
	gsl_matrix_fprintf(stdout, A, "%g");
	printf("R = \n");
	gsl_matrix_fprintf(stdout, R, "%g");
	gsl_matrix* result = matrix_matrix_product(A,R);
	printf("QR = \n");
	gsl_matrix_fprintf(stdout, result, "%g");
	gsl_matrix_free(result);
	
	//free memory
	gsl_matrix_free(A);
	gsl_matrix_free(R);
	*/
}


int main(int argc, char** argv){
	test();
	return 0;
}

