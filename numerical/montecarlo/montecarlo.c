#include<math.h>
#include<stdlib.h>
#include<assert.h>
#include"montecarlo.h"
#define RND ((double) rand() / RAND_MAX)

/* Generates a x_i in [a_i;b_i] */
void random_x(gsl_vector* a, gsl_vector* b, gsl_vector* x){
	int dim = a->size;
	assert(dim == b->size);
	assert(dim == x->size);
	for(int i = 0; i<dim; i++){
		double x_i = gsl_vector_get(a,i) + RND*(gsl_vector_get(b,i)-gsl_vector_get(a,i));
		gsl_vector_set(x, i, x_i);
	}
}

//Plain monte carlo integration of a function f
double montecarlo_plain(
	double f(gsl_vector* x),	//Function to integrate
	gsl_vector* a,			//Lower limit
	gsl_vector* b,			//Upper limit
	int N,				//Number of points
	double* error			//Estimated error
){
	double V = 1;
	double dim = a->size;
	gsl_vector* x = gsl_vector_alloc(dim);
	for(int i = 0; i < dim; i++){
		V *= gsl_vector_get(b,i)-gsl_vector_get(a,i);
	}
	double sum = 0, sum_sq = 0, fx;
	for(int i = 0; i < N; i++){		//Calculate function value at many random points
		random_x(a,b,x);
		fx = f(x);
		sum += fx;
		sum_sq += fx*fx;
	}
	double avr = sum / N;			 //Calculate mean function value
	double var = sum_sq / N - avr*avr;	 //Calculate variance
	*error = sqrt(var/N)*V;			 //Estimate error on integral estimate
	gsl_vector_free(x);
	return avr*V;				 //Integral estimation
}


