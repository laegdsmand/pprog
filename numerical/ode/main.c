#include"rk_ode.h"
#include<stdio.h>
#include<math.h>
#include<assert.h>
#ifndef M_PI
#define M_PI           3.14159265358979323846
#endif

void decay(double t, gsl_vector* yt, gsl_vector* dydt){
	gsl_vector_set(dydt,0,gsl_vector_get(yt,1));
	gsl_vector_set(dydt,1,-0.8*gsl_vector_get(yt,1)-2*gsl_vector_get(yt,0));	
}

double integrand(double x){
	assert(x != 1);
	return 2/sqrt(1-x*x);
}

double definite_integral(double f(double x), double a, double b){
	void diff_eq(double t, gsl_vector* y, gsl_vector* dydt){
		gsl_vector_set(dydt,0,f(t));	
	}
	gsl_matrix* path = gsl_matrix_alloc(9000,2);
	gsl_vector* yt = gsl_vector_alloc(1);
	gsl_vector_set(yt,0,0);
	double h = 0.1;
	driver(
	(&a),		
	b,	
	(&h),		
	yt,
	1e-4,                            /* absolute accuracy goal */
	1e-6,                            /* relative accuracy goal */
	rkstep12,	
	diff_eq,	
	path
	);
	double I = gsl_vector_get(yt,0);
	gsl_matrix_free(path);
	gsl_vector_free(yt);
	return I;
}

void test_ode_decay(){
double a = 0;
double h = 0.1;
gsl_matrix* path = gsl_matrix_alloc(2000,3);
gsl_vector* yt = gsl_vector_alloc(2);
gsl_vector_set(yt,0,10);
gsl_vector_set(yt,1,0);
int steps = driver(
	(&a),		
	10.0,	
	(&h),		
	yt,
	1e-4,                            /* absolute accuracy goal */
	1e-3,                            /* relative accuracy goal */
	rkstep12,	
	decay,	
	path
);
for(int i=0;i<steps; i++){
	double t = gsl_matrix_get(path,i,0);
	double y = gsl_matrix_get(path,i,1);
	double dydt = gsl_matrix_get(path,i,2);
	printf("%g\t%g\t%g\n",t,y,dydt);
}
printf("\n\n");
for(int i=0;i<steps;i++){
	
	double t = gsl_matrix_get(path,i,0);
	double y = exp(-0.4*t)*(2.94884*sin(1.35647*t)+10*cos(1.35647*t));
	printf("%g\t%g\n",t,y);
}
fprintf(stderr,"converged after %i steps\n",steps);
gsl_matrix_free(path);
gsl_vector_free(yt);
}

void test_integration(){
	printf("--- TESTING ODE SOLVING ---\n");
	printf("The decaying oscillation was solved and plotted in solution.svg\n and compared with the analytic solution\n");
	printf("---TESTING ODE SOLVER AS INTEGRATOR---\n");
	printf("-First target is sin(x) from 0 to pi\n");
	double I = definite_integral(sin,0,M_PI);
	printf("Which gives I = %.5g\n",I);
	printf("-Second target is 2/sqrt(1-x^2) from 0 to 1\n");
	I = definite_integral(integrand,0,1-1e-9);
	printf("Which gives I = %g\n",I);
}

int main(int argc, char** argv){
	if(argc>1){
		test_ode_decay();
	}else{
		test_integration();
	}
	return 0;
	
}
