#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include"rk_ode.h"

void rkstep12(
	double t,
	double h,
	gsl_vector* yt,
	void f(double t, gsl_vector* y, gsl_vector* dydt),
	gsl_vector* yth,
	gsl_vector* err
){
	int n=yt->size;	
	gsl_vector* k0 = gsl_vector_alloc(n);
	gsl_vector* k12 = gsl_vector_alloc(n);
	gsl_vector* yt2 = gsl_vector_alloc(n);	
	//Using the formulas in eq 14:
	f(t,yt,k0); // compute k0
	//compute stepped y for further calculation
	for(int i = 0; i < n; i++){
		double y2step = gsl_vector_get(yt,i)+gsl_vector_get(k0,i)*h/2;
		gsl_vector_set(yt2,i,y2step);
	}
	f(t+h/2,yt2,k12); //compute k_1/2
	for(int i = 0; i < n; i++){
		double yth_val = gsl_vector_get(yt,i)+gsl_vector_get(k12,i)*h;
		gsl_vector_set(yth,i,yth_val);
		//Calculating error estimate for y
		double err_val = (gsl_vector_get(k0,i)-gsl_vector_get(k12,i))*h/2;
		gsl_vector_set(err,i,err_val);
	}
	
	
	
	gsl_vector_free(yt2);
	gsl_vector_free(k0);
	gsl_vector_free(k12);	
	
}


int driver(
	double* a,                             /* the start-point of integration */
	double b,                              /* the end-point of the integration */
	double* h,                             /* the current step-size */
	gsl_vector* yt,                             /* the current y(t) */
	double acc,                            /* absolute accuracy goal */
	double eps,                            /* relative accuracy goal */
	void stepper(                          /* the stepper function to be used */
		double t, double h, gsl_vector*yt,
		void f(double t,gsl_vector* y,gsl_vector* dydt),
		gsl_vector* yth, gsl_vector* err
		),
	void f(double t,gsl_vector* y,gsl_vector* dydt), /* right-hand-side */
	gsl_matrix* path
){
	int n = yt->size;
	int n_steps = 0,step_limit=path->size1;
	double t=(*a);
	gsl_vector* yth = gsl_vector_alloc(n); // The next step
	gsl_vector* err = gsl_vector_alloc(n); // Estimated error
	while(t < b){ //Until endpoint reached
	
		gsl_matrix_set(path,n_steps,0,t);
		for(int i = 0; i < n; i++){
			gsl_matrix_set(path,n_steps,i+1, gsl_vector_get(yt,i));
		}
		if(t+(*h)>b){ (*h)=b-t;}
		rkstep12(t,(*h),yt,f,yth,err); //Take step
		double norm_error = gsl_blas_dnrm2(err); //absolute local error
		double norm_yth	  = gsl_blas_dnrm2(yth); //norm of new y
		double tol	  = (eps*norm_yth+acc)*sqrt((*h)/(b-(*a)));
		
		if(norm_error<tol){ //Step gets accepted otherwise try with new h
			n_steps++;
			if(n_steps >= step_limit-1) break;//No more memory for storing path
			t += (*h); // increase current t
			gsl_vector_memcpy(yt,yth); // and y(t)
		}
		if(norm_error>0){//Adjust step size
			(*h) *= pow(tol/norm_error,0.25)*0.95; 
		}else{//no need to take such small steps
			(*h) *= 2;
		}
	}
	gsl_matrix_set(path,n_steps,0,t);
	for(int i = 0; i < n; i++){
		gsl_matrix_set(path,n_steps,i+1, gsl_vector_get(yt,i));
	}
	(*a) = t;
	gsl_vector_free(yth);
	gsl_vector_free(err);
	return n_steps;
}
