#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>

void rkstep12(
	double t,
	double h,
	gsl_vector* yt,
	void f(double t, gsl_vector* y, gsl_vector* dydt),
	gsl_vector* yth,
	gsl_vector* err
);

int driver(
	double* a,                             /* the start-point of integration */
	double b,                              /* the end-point of the integration */
	double* h,                             /* the current step-size */
	gsl_vector* yt,                             /* the current y(t) */
	double acc,                            /* absolute accuracy goal */
	double eps,                            /* relative accuracy goal */
	void stepper(                          /* the stepper function to be used */
		double t, double h, gsl_vector* yt,
		void f(double t,gsl_vector* y,gsl_vector* dydt),
		gsl_vector* yth, gsl_vector* err
		),
	void f(double t,gsl_vector* y,gsl_vector* dydt), /* right-hand-side */
	gsl_matrix* path
);
