#include"aux_linalg.h"
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<assert.h>
#include<stdio.h>
#include<math.h>

//Computes A <-  v * u^T
void gsl_vector_outer_product(gsl_vector* v, gsl_vector* u, gsl_matrix* A){
	assert(v->size == A->size1);
	assert(u->size == A->size2);
	for(int i = 0; i < v->size; i++){
		for(int j = 0; j < u->size; j++){
			gsl_matrix_set(A,i,j,
				gsl_vector_get(v,i)*gsl_vector_get(u,j)
			);	
		}
	}
}

double gsl_vector_norm(gsl_vector* v){
	double norm = 0;
	for(int i = 0; i<v->size; i++){
		norm += gsl_vector_get(v,i)*gsl_vector_get(v,i);
	}
	norm = sqrt(norm);
	return norm;
}

double gsl_vector_dot_product(gsl_vector* v, gsl_vector* u){
	assert(v->size == u->size);
	double dp = 0;
	for(int i = 0; i < v->size; i++){
		dp += gsl_vector_get(v,i)*gsl_vector_get(u,i);
	}
	return dp;
}


void print_matrix_pretty(gsl_matrix *A){
      for(int i = 0;i<A->size1; i++){
           printf("[");
                 for(int j = 0;j<A->size2;j++){
                         printf("%.3e  ",gsl_matrix_get(A,i,j));
                 }
                 printf("]\n");
         }
 } 

