#ifndef AUX_LINALG
#define AUX_LINALG
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
void gsl_vector_outer_product(gsl_vector* v, gsl_vector* u, gsl_matrix* A);
double gsl_vector_dot_product(gsl_vector* v, gsl_vector* u);
double gsl_vector_norm(gsl_vector* v);
void print_matrix_pretty(gsl_matrix *A);

#endif
