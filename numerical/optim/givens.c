#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include<assert.h>
#include"givens.h"



void givens_qr_decomp(gsl_matrix* A){
	int n = A->size2;
	int m = A->size1;
	assert(m>=n);
	for(int q = 0; q < n; q++){
		for(int p = q+1; p < m; p++){
			//apply givens rotation on element below diagonal
			double theta = atan2(
				gsl_matrix_get(A,p,q), gsl_matrix_get(A,q,q));
			for(int k = q; k < n; k++){
				double x_q = gsl_matrix_get(A,q,k);
				double x_p = gsl_matrix_get(A,p,k);
				gsl_matrix_set(A,q,k, x_q*cos(theta) + x_p*sin(theta));
				gsl_matrix_set(A,p,k,-x_q*sin(theta) + x_p*cos(theta));
			}
			gsl_matrix_set(A,p,q,theta);
		}
	}
}

//Apply Q^T (or G) to vector v
void givens_qr_qtrans_vector(gsl_matrix* QR, gsl_vector* v){
	int n = QR->size2, m = QR->size1;
	assert(v->size == m);
	for(int q = 0; q < n; q++)for(int p=q+1; p < m; p++){
		//We apply all the rotations to their respective elements in v
		double theta = gsl_matrix_get(QR,p,q);
		double v_q = gsl_vector_get(v,q), v_p = gsl_vector_get(v,p);
		gsl_vector_set(v,q, v_q*cos(theta) + v_p*sin(theta));
		gsl_vector_set(v,p,-v_q*sin(theta) + v_p*cos(theta));		
	}
}

void givens_qr_get_Q(gsl_matrix* QR,gsl_matrix* Q){
	int n = QR->size2, m = QR->size1;
	gsl_vector* e_i = gsl_vector_alloc(m);
	for(int i = 0; i < m; i++){
		gsl_vector_set_basis(e_i, i);
		givens_qr_qtrans_vector(QR,e_i);
		for(int j = 0; j < n; j++){
			gsl_matrix_set(Q,i,j,gsl_vector_get(e_i, j));
		}
		
	}
	gsl_vector_free(e_i);
}

void givens_qr_get_R(gsl_matrix* QR, gsl_matrix* R){
	gsl_matrix_memcpy(R,QR);
	int n = QR->size2, m = QR->size1;

	for(int q = 0; q < n; q++)for(int p=q+1; p < m; p++ ){
		gsl_matrix_set(R,p,q, 0);
	}
}

//Solve the triangular system Tx = b by backsubstitution, then b<-x
void backsub(gsl_matrix* T,gsl_vector* b){
	int n = b->size;
	for(int i = n-1; i>=0; i-- ){
		double x_i = gsl_vector_get(b,i);
		for(int j = n-1; j>i; j--){
			x_i -= gsl_matrix_get(T,i,j)*gsl_vector_get(b,j);
		}
		x_i = x_i / gsl_matrix_get(T,i,i);
		gsl_vector_set(b,i,x_i);
	}
}

void givens_qr_solve(gsl_matrix* QR, gsl_vector* b){
	givens_qr_qtrans_vector(QR,b);
	backsub(QR,b);

}

