#ifndef GIVENS
#define GIVENS
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>

void givens_qr_decomp(gsl_matrix* A);

void givens_qr_qtrans_vector(gsl_matrix* QR, gsl_vector* v);
void givens_qr_get_Q(gsl_matrix* QR,gsl_matrix* Q);

void givens_qr_get_R(gsl_matrix* QR, gsl_matrix* R);

void givens_qr_solve(gsl_matrix* QR, gsl_vector* b);
#endif
