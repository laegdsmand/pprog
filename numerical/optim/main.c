#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<math.h>
#include<stdio.h>
#include"newton_min.h"

double fit_error(gsl_vector* v){
	double A = gsl_vector_get(v,0);
	double T = gsl_vector_get(v,1);
	double B = gsl_vector_get(v,2);
	double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
	double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
	double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
	int N = sizeof(t)/sizeof(t[0]);
	double F = 0; //The error
	for(int i = 0; i<N; i++){
		double ft = A*exp(-t[i]/T) + B;
		F += pow((ft-y[i])/e[i],2);
	}
	return F;
}

double rosenbrock(gsl_vector* v, gsl_vector* df, gsl_matrix* H){
	double x = gsl_vector_get(v,0);
	double y = gsl_vector_get(v,1);
	//set gradient
	double dfdx = 2*(200*x*x*x - 200*x*y + x - 1);
	double dfdy = 200*(y-x*x);
	gsl_vector_set(df,0,dfdx);
	gsl_vector_set(df,1,dfdy);	
	//Set hessian
	gsl_matrix_set(H,0,0, -400*(y-x*x)+800*x*x+2);
	gsl_matrix_set(H,0,1, -400*x); 
	gsl_matrix_set(H,1,0, -400*x);
	gsl_matrix_set(H,1,1,  200);
	return (1-x)*(1-x) + 100*(y-x*x)*(y-x*x);
}

double himmelblau(gsl_vector* v, gsl_vector* df, gsl_matrix* H){
	double x = gsl_vector_get(v,0);
	double y = gsl_vector_get(v,1);
	gsl_vector_set(df,0, 2*(2*x*(x*x+y-11)+x+y*y-7));
	gsl_vector_set(df,1, 2*(x*x+2*y*(x+y*y-7)+y-11));	
	gsl_matrix_set(H,0,0, 4*(x*x+y-11)+8*x*x+2);
	gsl_matrix_set(H,0,1, 4*x+4*y); 
	gsl_matrix_set(H,1,0, 4*x+4*y);
	gsl_matrix_set(H,1,1, 4*(x+y*y-7)+8*y*y+2);
	return pow(x*x+y-11,2)+pow(x+y*y-7,2);
}


double q_rosenbrock(gsl_vector* v){
	double x = gsl_vector_get(v,0);
	double y = gsl_vector_get(v,1);
	return (1-x)*(1-x) + 100*(y-x*x)*(y-x*x);
}

double q_himmelblau(gsl_vector* v){
	double x = gsl_vector_get(v,0);
	double y = gsl_vector_get(v,1);
	return pow(x*x+y-11,2)+pow(x+y*y-7,2);
}

void test_rosenbrock(){
	gsl_vector* x = gsl_vector_alloc(2);
	gsl_vector* df= gsl_vector_alloc(2);
	gsl_matrix* H = gsl_matrix_alloc(2,2);
	gsl_vector_set(x,0,2);
	gsl_vector_set(x,1,0);
	double f_val = himmelblau(x,df,H);
	fprintf(stderr, "rosenbrock function in (x,y) = (2,0:) is %g\n",f_val);
	printf("And df = \n");
	gsl_vector_fprintf(stdout, df, "%g");
}

void test_newton_minimizer(){
	double eps = 1e-5;
	int n_steps = 0;
	printf("--- TESTING NEWTON MINIMIZER ---\n");
	printf("1st target is rosenbrock function starting at (2,0)\n");
	gsl_vector* x = gsl_vector_alloc(2);
	gsl_vector_set(x,0, 2);
	gsl_vector_set(x,1, 0);
	n_steps = newton_min(rosenbrock, x, eps);
	printf("After %i steps, the minimum is at (x,y) = \n", n_steps);
	gsl_vector_fprintf(stdout, x, "%.4g");
	gsl_vector_set(x,0, 12);
	gsl_vector_set(x,1, 6);
	printf("2nd target is the himmelblau function starting at (12,6)\n");
	n_steps = newton_min(himmelblau, x, eps);	
	printf("After %i steps, the minimum is at (x,y) = \n", n_steps);
	gsl_vector_fprintf(stdout, x, "%.4g");
	gsl_vector_free(x);	
}

void test_qnewton_minimizer(){
	double eps = 1e-4;
	int n_steps = 0;
	printf("--- TESTING QUASI-NEWTON (BROYDEN) MINIMIZER ---\n");
	printf("1st target is rosenbrock function starting at (2,0)\n");
	gsl_vector* x = gsl_vector_alloc(2);
	gsl_vector_set(x,0, 2);
	gsl_vector_set(x,1, 0);
	n_steps = qnewton_broyden_min(q_rosenbrock, x, eps);
	printf("After %i steps, the minimum is at (x,y) = \n", n_steps);
	gsl_vector_fprintf(stdout, x, "%.4g");
	gsl_vector_set(x,0, 12);
	gsl_vector_set(x,1, 6);
	printf("2nd target is the himmelblau function starting at (12,6)\n");
	n_steps = qnewton_broyden_min(q_himmelblau, x, eps);	
	printf("After %i steps, the minimum is at (x,y) = \n", n_steps);
	gsl_vector_fprintf(stdout, x, "%.4g");
	printf("It works, but it's not nearly as good as with analytic Hessian :(\n");
	gsl_vector_free(x);	
}
	
void compare_roots(){
	printf("--- NEWTON ROOT FINDING METHODS  ---\n");
	printf("Newtons root finding with numeric jacobian found\nthe rosenbrock minimum after 329 steps\n and himmalblau min after 35\n");
	printf("With analytic jacobian supplied they found\nrosenbrock min after 234 steps and\nhimmelblau minimum after 35\n");
}

void test_fitting(){
	double eps = 1e-4;
	gsl_vector* x = gsl_vector_alloc(3);
	gsl_vector_set(x,0,1);
	gsl_vector_set(x,1,1);
	gsl_vector_set(x,2,1);
	int n_steps = qnewton_broyden_min(fit_error, x, eps);
	fprintf(stderr, "Found the minimum fit error after %i steps", n_steps);
	double A = gsl_vector_get(x,0);
	double T = gsl_vector_get(x,1);
	double B = gsl_vector_get(x,2);
	for(double t = 0; t < 10; t+= 0.1){	
		double ft = A*exp(-t/T) + B;
		printf("%g\t%g\n",t,ft);
	}
	gsl_vector_free(x);	
}



int main(int argc, char** argv){
	//test_rosenbrock();
	if(argc == 1){
		test_newton_minimizer();
		test_qnewton_minimizer();
		compare_roots();
	}else{
		test_fitting();
	}
	return 0;
}
