#include"newton_min.h"
#include"givens.h"
#include"aux_linalg.h"
#include<gsl/gsl_blas.h>

void gradient(
	double f(gsl_vector* x),
	gsl_vector* x,
	gsl_vector* df,
	double dx
){	
	double f_val = f(x);
	//Calculate gradient numerically
	for(int i = 0; i < x->size; i++){
		double x_i = gsl_vector_get(x,i);
		gsl_vector_set(x,i, x_i + dx);
		double dfdxi = (f(x)-f_val)/dx;
		gsl_vector_set(x,i, x_i);
		gsl_vector_set(df,i,dfdxi);
	}
}

int qnewton_broyden_min(
	double f(gsl_vector* x),
	gsl_vector* x,
	double eps
){
	
	double alpha = 0.01;
	size_t dim = x->size,n_steps = 0;
	double lambda = 2, dx = 1e-5;
	gsl_vector* s = gsl_vector_calloc(dim);
	gsl_vector* u = gsl_vector_calloc(dim);
	gsl_vector* y = gsl_vector_calloc(dim);
	gsl_vector* Delta_x = gsl_vector_calloc(dim);
	gsl_vector* x_step = gsl_vector_calloc(dim);
	gsl_vector* df = gsl_vector_calloc(dim);
	gsl_matrix* B = gsl_matrix_calloc(dim,dim);
	gsl_matrix* dB = gsl_matrix_calloc(dim,dim);
	gsl_matrix_set_identity(B);
	do{
	//Compute f
	double f_val = f(x);
	gradient(f,x,df,dx);
	//Calculate step
	gsl_blas_dgemv(CblasNoTrans,-1,B,df,0,Delta_x);
	//printf("Delta x = \n");
	//gsl_vector_fprintf(stdout, Delta_x, "%g");
	//For backtracking
	int armijo_condition=1;
	lambda = 2.0;
	do{
		lambda /= 2.0;
		gsl_vector_memcpy(s,Delta_x);
		gsl_vector_scale(s,lambda);
		gsl_vector_memcpy(x_step,x);
		gsl_vector_add(x_step,s);
		double f_step = f(x_step);
		double dp = alpha * gsl_vector_dot_product(s,df);
		if(gsl_vector_norm(s)<dx*1e-1){
			armijo_condition=0;
			gsl_matrix_set_identity(B);
			//printf("SET B to IDENTITY");
		}
		armijo_condition = (f_step >= f_val+dp) & armijo_condition;
	}while(armijo_condition);
	//Calculate u = s - By
	gradient(f,x_step,y,dx);
	gsl_vector_sub(y,df);
	gsl_vector_memcpy(u,s);
	gsl_blas_dgemv(CblasNoTrans, -1, B, y, 1, u);
	//Calculate u <- c = u / ( s^T * y )
	double sTy = gsl_vector_dot_product(s,y);
	if(sTy*sTy > 1e-6){
		gsl_vector_scale(u,1/sTy);
		//update B
		gsl_vector_outer_product(u,s,dB);
		gsl_matrix_add(B,dB);
	}
	gsl_vector_memcpy(x,x_step);
	if(gsl_vector_norm(df)<eps) break;	
	//printf("norm df = %g\n", gsl_vector_norm(df));
	n_steps++;
	}while((gsl_vector_norm(df)>eps) & (n_steps < 500) /*convergence?*/);
	
	
	
	gsl_vector_free(s);
	gsl_vector_free(u);
	gsl_vector_free(y);
	gsl_vector_free(x_step);
	gsl_vector_free(Delta_x);
	gsl_vector_free(df);
	gsl_matrix_free(B);
	gsl_matrix_free(dB);
	return n_steps;
}

int newton_min(
	double f(gsl_vector* x, gsl_vector* df, gsl_matrix* H),
	gsl_vector* x_start,
	double eps
){
	double alpha = 2e-4;
	size_t dim = x_start->size,n_steps = 0;
	double lambda = 2;
	gsl_vector* s = gsl_vector_alloc(dim);
	gsl_vector* Delta_x = gsl_vector_alloc(dim);
	gsl_vector* x_step = gsl_vector_alloc(dim);
	gsl_vector* df = gsl_vector_alloc(dim);
	gsl_matrix* H = gsl_matrix_alloc(dim,dim);
	do{
	//Compute f, derivative and Hessian matrix
	
	double f_val = f(x_start,df,H);	
	//gsl_vector_fprintf(stdout,df,"%g");
	//printf(" f = %g, doing system solvez\n", f_val);
	//Solve the linear system del f(x) + H(x)*Delta_x = 0 
	givens_qr_decomp(H);
	gsl_vector_memcpy(Delta_x, df);
	//Equivalent to H*Delta_x = -df
	gsl_vector_scale(Delta_x, -1);
	givens_qr_solve(H,Delta_x);	
	//printf("starting backtracking with Delta \n");
	//gsl_vector_fprintf(stdout, Delta_x, "%g");
	//For backtracking
	//printf("Delta x = \n");
	//gsl_vector_fprintf(stdout, Delta_x, "%g");
	int armijo_condition;
	lambda = 2.0;
	do{
		lambda /= 2.0;
		gsl_vector_memcpy(s,Delta_x);
		gsl_vector_scale(s,lambda);
		gsl_vector_memcpy(x_step,x_start);
		gsl_vector_add(x_step,s);
		double f_step = f(x_step,df,H);
		double dp = alpha * gsl_vector_dot_product(s,df);
		armijo_condition = (f_step >= f_val+dp) & (lambda > 1.0/64.0);
	}while(armijo_condition);
	//gsl_vector_fprintf(stdout, s, "%g");
	//printf("lambda = %g",lambda);
	gsl_vector_memcpy(x_start,x_step);
	n_steps++;
	}while((gsl_vector_norm(df)>eps) & (n_steps < 100) /*convergence?*/);
	//Free used memory
	gsl_vector_free(s);
	gsl_vector_free(x_step);
	gsl_vector_free(Delta_x);
	gsl_vector_free(df);
	gsl_matrix_free(H);
	return n_steps;
}

