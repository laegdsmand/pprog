#ifndef NEWTON_MIN
#define NEWTON_MIN
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>


/* f:		objective function to optimize
   x_start:	starting point for minimization
   eps:		accuracy goal, on exit |gradient| < eps 
*/
int newton_min(
	double f(gsl_vector* x, gsl_vector* df, gsl_matrix* H),
	gsl_vector* x_start,
	double eps
);

int qnewton_broyden_min(
	double f(gsl_vector* x),
	gsl_vector* x,
	double eps
);

int qNewton(
	double f(gsl_vector* x),
	gsl_vector* x,
	double eps
);
#endif
