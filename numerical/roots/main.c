#include"rootsA.h"
#include"givens.h"
#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_multiroots.h>
#include<math.h>


void f_1(gsl_vector* x, gsl_vector* fx){
	gsl_vector_memcpy(fx,x);
}

void f_system(gsl_vector* v, gsl_vector* fv){
	double A = 10000;
	double x = gsl_vector_get(v,0);
	double y = gsl_vector_get(v,1);
	double f1 = A*x*y-1;
	double f2 = exp(-x)+exp(-y)-1-1/A;
	gsl_vector_set(fv,0,f1);
	gsl_vector_set(fv,1,f2);
}

void f_system_J(gsl_vector* v, gsl_vector* fv, gsl_matrix* J){
	double A = 10000;
	f_system(v,fv);
	double x = gsl_vector_get(v,0);
	double y = gsl_vector_get(v,1);
	double df1dx = A*y, df1dy = A*x;
	double df2dx = -exp(-x), df2dy = -exp(-y);
	gsl_matrix_set(J,0,0,df1dx);
	gsl_matrix_set(J,0,1,df1dy);
	gsl_matrix_set(J,1,0,df2dx);
	gsl_matrix_set(J,1,1,df2dy);

}
int f_system_gsl(const gsl_vector* v,void* params, gsl_vector* fv){
	f_system(v,fv);
	return GSL_SUCCESS;
}


void d_rosenbrock(gsl_vector* v, gsl_vector* fv){
	double x = gsl_vector_get(v,0);
	double y = gsl_vector_get(v,1);
	double dfdx = -2*(1-x) + 200*(y-x*x)*((-2)*x);
	double dfdy = 200*(y-x*x);
	gsl_vector_set(fv,0,dfdx);
	gsl_vector_set(fv,1,dfdy);
}

void d_rosenbrock_J(gsl_vector* v, gsl_vector* fv, gsl_matrix* J){
	double x = gsl_vector_get(v,0);
	double y = gsl_vector_get(v,1);
	d_rosenbrock(v,fv);
	double df1dx = 2-400*(y-3*x*x), df1dy = -400*x;
	double df2dx = -400*x, df2dy = 200;
	gsl_matrix_set(J,0,0,df1dx);
	gsl_matrix_set(J,0,1,df1dy);
	gsl_matrix_set(J,1,0,df2dx);
	gsl_matrix_set(J,1,1,df2dy);
}

int d_rosenbrock_gsl(const gsl_vector* v, void* params, gsl_vector* fv){
	d_rosenbrock(v,fv);
	return GSL_SUCCESS;
}

void d_himmelblau(gsl_vector* v, gsl_vector* fv){
	double x = gsl_vector_get(v,0);
	double y = gsl_vector_get(v,1);
	double dfdx = 2*(x*x+y-11)*(2*x) + 2*(x+y*y-7);
	double dfdy = 2*(x*x+y-11) + 2*(x+y*y-7)*(2*y);
	gsl_vector_set(fv,0,dfdx);
	gsl_vector_set(fv,1,dfdy);
}
void d_himmelblau_J(gsl_vector* v, gsl_vector* fv,gsl_matrix* J){
	double x = gsl_vector_get(v,0);
	double y = gsl_vector_get(v,1);
	d_himmelblau(v,fv);
	double df1dx = 12*x*x+4*y-42, df1dy = 4*x+4*y;
	double df2dx = 4*x+4*y, df2dy = 4*x+12*y*y-26;
	gsl_matrix_set(J,0,0,df1dx);
	gsl_matrix_set(J,0,1,df1dy);
	gsl_matrix_set(J,1,0,df2dx);
	gsl_matrix_set(J,1,1,df2dy);
}

int d_himmelblau_gsl(const gsl_vector* v, void* params, gsl_vector* fv){
	d_himmelblau(v,fv);
	return GSL_SUCCESS;
}
void test_jacobian_numeric(){
	int n = 2,n_calls = 0;
	double data[2] = {2,2};
	double dx = 0.1;
	gsl_vector_view x = gsl_vector_view_array(data,n);
	gsl_vector* fx = gsl_vector_alloc(n);
	gsl_matrix* J = gsl_matrix_alloc(n,n);
	gsl_vector* fz = gsl_vector_alloc(n);
	jacobian_numerical(J,&f_system,&x.vector,fx,fz,dx,&n_calls);
	print_matrix_pretty(J);
}

void test_newton(){
	printf("--- TESTING NEWTON WITH NUMERIC JACOBIAN ---\n");
	int n = 2,n_steps=0,n_calls=0;
	//Find minimum of rosenbrock function
	double x_start[2] = {2,0};
	double dx = 0.001, eps = 1e-6;
	gsl_vector_view x = gsl_vector_view_array(x_start,n);
	newton(&d_rosenbrock, &x.vector, dx, eps,&n_steps,&n_calls);
	fprintf(stdout, "Root search to find the minimum of rosenbrock function\n f(x,y) = (1-x)^2 + 100(y-x^2)^2, x = \n");
	gsl_vector_fprintf(stdout,&x.vector, "%.4g");
	printf("It took %i steps and %i function calls\n", n_steps, n_calls);

	//Find minimum of himmelblau function
	gsl_vector_set(&x.vector,0,12);
	gsl_vector_set(&x.vector,1,6);
	newton(&d_himmelblau, &x.vector, dx, eps,&n_steps,&n_calls);
	fprintf(stdout, "\nRoot search to find the minimum of himmelblau function, x_min = \n");
	gsl_vector_fprintf(stdout,&x.vector, "%.4g");
	printf("It took %i steps and %i function calls\n", n_steps, n_calls);

	//Solve the system of equations!
	gsl_vector_set(&x.vector,0,2);
	gsl_vector_set(&x.vector,1,1);
	newton(&f_system, &x.vector, dx, eps,&n_steps,&n_calls);
	fprintf(stdout, "\nRoot search to find solution to system of equations \n");
	fprintf(stdout, "A*x*y = 1, exp(-x)+exp(-y) = 1+1/A, A = 10000\n (x,y) = \n");
	gsl_vector_fprintf(stdout,&x.vector, "%.4g");
	printf("It took %i steps and %i function calls\n", n_steps, n_calls);

}

void test_newton_analytic(){
	printf("\n--- TESTING NEWTON WITH ANALYTICAL JACOBIAN ----\n");
	int n = 2,n_steps=0,n_calls=0;
	//Find minimum of rosenbrock function
	double x_start[2] = {2,0};
	double dx = 1e-2, eps = 1e-6;
	gsl_vector_view x = gsl_vector_view_array(x_start,n);
	newton_analytic(&d_rosenbrock_J, &x.vector, dx, eps,&n_steps,&n_calls);
	fprintf(stdout, "Root search to find the minimum of rosenbrock function\n f(x,y) = (1-x)^2 + 100(y-x^2)^2, x = \n");
	gsl_vector_fprintf(stdout,&x.vector, "%.4g");
	printf("It took %i steps and %i function calls\n", n_steps, n_calls);
	
	//Find minimum of himmelblau function
	gsl_vector_set(&x.vector,0,12);
	gsl_vector_set(&x.vector,1,6);
	newton_analytic(&d_himmelblau_J, &x.vector, dx, eps,&n_steps,&n_calls);
	fprintf(stdout, "\nRoot search to find the minimum of himmelblau function, x_min = \n");
	gsl_vector_fprintf(stdout,&x.vector, "%.4g");
	printf("It took %i steps and %i function calls\n", n_steps, n_calls);
	//Solve the system of equations!
	gsl_vector_set(&x.vector,0,2);
	gsl_vector_set(&x.vector,1,1);
	newton_analytic(&f_system_J, &x.vector, dx, eps,&n_steps,&n_calls);
	fprintf(stdout, "\nRoot search to find solution to system of equations \n");
	fprintf(stdout, "A*x*y = 1, exp(-x)+exp(-y) = 1+1/A, A = 10000\n (x,y) = \n");
	gsl_vector_fprintf(stdout,&x.vector, "%.4g");
	printf("It took %i steps and %i function calls\n", n_steps, n_calls);
}


/* Routine which initializes gsl_multiroot_fsolver and finds root for system f */
void gsl_root_finding(
	int (*f)(const gsl_vector* x, void* params, gsl_vector* fx),
	gsl_vector* x,
	int* n_steps,
	double eps
){
	(*n_steps) = 0;
	gsl_multiroot_fsolver *s = gsl_multiroot_fsolver_alloc(
		gsl_multiroot_fsolver_dnewton,
		x->size
	);
	gsl_multiroot_function rf;
	rf.f = f;
	rf.n = x->size;
	rf.params = NULL;
	gsl_multiroot_fsolver_set(s,&rf,x);
	int status;
	do{
		(*n_steps)++;
		status = gsl_multiroot_fsolver_iterate(s);
		if(status) break;
		status = gsl_multiroot_test_residual(s->f,eps);
	}while(status == GSL_CONTINUE && (*n_steps) < 10000);
	for(int i = 0; i < x->size;i++){
		gsl_vector_set(x,i,
			gsl_vector_get(s->x, i)
		);
	}	
	gsl_multiroot_fsolver_free(s);
}

void test_root_gsl(){
	printf("\n--- TESTING GSL ROOT FINDER USING DNEWTON SOLVER ---\n");	
	int n = 2,n_steps=0;
	//Find minimum of rosenbrock function
	double x_start[2] = {2,5};
	double eps = 1e-6;
	gsl_vector_view x = gsl_vector_view_array(x_start,n);
	gsl_root_finding(&d_rosenbrock_gsl, &x.vector,&n_steps,eps);
	fprintf(stdout, "Root search to find the minimum of rosenbrock function\n f(x,y) = (1-x)^2 + 100(y-x^2)^2, x = \n");
	gsl_vector_fprintf(stdout,&x.vector, "%.4g");
	
	printf("It took %i steps\n", n_steps);
	
	//Find minimum of himmelblau function
	gsl_vector_set(&x.vector,0,14);
	gsl_vector_set(&x.vector,1,7);
	gsl_root_finding(&d_himmelblau_gsl, &x.vector,&n_steps,eps);
	fprintf(stdout, "\nRoot search to find the minimum of himmelblau function, x_min = \n");
	gsl_vector_fprintf(stdout,&x.vector, "%.4g");
	
	printf("It took %i steps\n", n_steps);
	//Solve the system of equations!
	gsl_vector_set(&x.vector,0,2);
	gsl_vector_set(&x.vector,1,1);
	gsl_root_finding(&f_system_gsl, &x.vector, &n_steps,eps);
	fprintf(stdout, "\nRoot search to find solution to system of equations \n");
	fprintf(stdout, "A*x*y = 1, exp(-x)+exp(-y) = 1+1/A, A = 10000\n (x,y) = \n");
	gsl_vector_fprintf(stdout,&x.vector, "%.4g");
	printf("It took %i steps\n", n_steps);
}

int main(){
	//test_jacobian_numeric();
	test_newton();
	test_newton_analytic();
	test_root_gsl();
}
