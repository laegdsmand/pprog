#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_linalg.h>
#include<math.h>
#include"givens.h"
#include"rootsA.h"

double gsl_vector_norm(gsl_vector* v){
	double norm = 0;
	for(int i = 0; i<v->size; i++){
		norm += gsl_vector_get(v,i)*gsl_vector_get(v,i);
	}
	norm = sqrt(norm);
	return norm;
}


void jacobian_numerical(gsl_matrix* J,
	void f(gsl_vector* x, gsl_vector* fx),
	gsl_vector* x,
	gsl_vector* fx,
	gsl_vector* fz,
	double dx,
	int* n_calls)
{
	int n = x->size;
	f(x,fx); //compute f(x) and store in fx
	(*n_calls)++;
	for(int j = 0; j<n; j++){
		// Update x (z) by adding dx to x_j
		gsl_vector_set(x,j,gsl_vector_get(x,j)+dx);
		f(x,fz); //fz = f(z)
		(*n_calls)++;
		gsl_vector_sub(fz,fx); //calculate fz =  f(x+dx)-f(x)
		for(int i = 0; i < n; i++){
			gsl_matrix_set(J,i,j,
				gsl_vector_get(fz,i) / dx
			);
		}
		gsl_vector_set(x,j,gsl_vector_get(x,j) - dx);
	}
}

/* Find the root of multivariate function f, using newtons method with backtracking
and numerical jacobian
  f should be a function which takes input vector x and computes f(x)
  x is the starting point or the root finding algorithm
 */
void newton(
	void f(gsl_vector* x, gsl_vector* fx),
	gsl_vector* x,
	double dx,
	double epsilon,
	int* n_steps,
	int* n_calls
){
	int n = x->size, condition;
	(*n_steps) = 0; (*n_calls) = 0;
	gsl_matrix* J = gsl_matrix_alloc(n,n);
	gsl_vector* z = gsl_vector_alloc(n);
	gsl_vector* fx= gsl_vector_alloc(n);
	gsl_vector* Dx= gsl_vector_alloc(n);//Delta x
	gsl_vector* fz= gsl_vector_alloc(n);

	do{

		jacobian_numerical(J,f,x,fx,fz,dx,n_calls);

		givens_qr_decomp(J);

		//Solve the linear system J*x = fx
		gsl_vector_memcpy(Dx, fx);
		gsl_vector_scale(Dx,-1);
		givens_qr_solve(J,Dx);
		double s = 1;
		do{
			for(int i = 0;i<n; i++){
				gsl_vector_set(z,i,
					gsl_vector_get(x,i) + gsl_vector_get(Dx,i)
				);
			}
			f(z,fz);(*n_calls)++;
			f(x,fx);(*n_calls)++;
			gsl_vector_scale(fx,1.0 - s/2.0);
			gsl_vector_scale(Dx,0.5);
			s*=0.5;
			condition = (gsl_vector_norm(fz)>gsl_vector_norm(fx));
			condition = condition & (s>1.0/64.0);
		}while(condition);
		gsl_vector_add(x,Dx);
		f(x,fx);
		//For tracking x
		//fprintf(stderr, "made increment to x = (%g,%g)\n", gsl_vector_get(x,0),gsl_vector_get(x,1));
		(*n_steps)++;
	}while((gsl_vector_norm(fx) > epsilon)&((*n_steps)<10000));

	//Free used memory
	gsl_matrix_free(J);
	gsl_vector_free(z);
	gsl_vector_free(Dx);
	gsl_vector_free(fx);
	gsl_vector_free(fz);
}


/* Finds the root of system using newtons method for a function which supplies
	The analutical Jacobian for the system
 */
void newton_analytic(
	void f(gsl_vector* x, gsl_vector* fx,gsl_matrix* J),
	gsl_vector* x,
	double dx,
	double epsilon,
	int* n_steps,
	int* n_calls
){
	int n = x->size, condition;
	(*n_steps) = 0; (*n_calls) = 0;
	gsl_matrix* J = gsl_matrix_alloc(n,n);
	gsl_vector* z = gsl_vector_alloc(n);
	gsl_vector* fx= gsl_vector_alloc(n);
	gsl_vector* Dx= gsl_vector_alloc(n);//Delta x
	gsl_vector* fz= gsl_vector_alloc(n);

	do{
		f(x,fx,J); (*n_calls)++;
		givens_qr_decomp(J);

		//Solve the linear system J*x = fx
		gsl_vector_memcpy(Dx, fx);
		gsl_vector_scale(Dx,-1);
		givens_qr_solve(J,Dx);
		double s = 1;
		do{
			for(int i = 0;i<n; i++){
				gsl_vector_set(z,i,
					gsl_vector_get(x,i) + gsl_vector_get(Dx,i)
				);
			}
			f(z,fz,J);(*n_calls)++;
			f(x,fx,J);(*n_calls)++;
			gsl_vector_scale(fx,1.0 - s/2.0);
			gsl_vector_scale(Dx,0.5);
			s*=0.5;
			condition = (gsl_vector_norm(fz)>gsl_vector_norm(fx));
			condition = condition & (s>1.0/64.0);
		}while(condition);
		gsl_vector_add(x,Dx);
		f(x,fx,J);
		//For tracking x
		//fprintf(stderr, "made increment to x = (%g,%g)\n", gsl_vector_get(x,0),gsl_vector_get(x,1));
		(*n_steps)++;	
	}while((gsl_vector_norm(fx) > epsilon)&((*n_steps)<10000));

	//Free used memory
	gsl_matrix_free(J);
	gsl_vector_free(z);
	gsl_vector_free(Dx);
	gsl_vector_free(fx);
	gsl_vector_free(fz);
}
