#ifndef ROOTS
#define ROOTS
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>

void jacobian_numerical(
	gsl_matrix* J,
	void f(gsl_vector* x, gsl_vector* fx),
	gsl_vector* x,
	gsl_vector* fx,
	gsl_vector* fz,
	double dx,
	int* n_calls
);
void newton(
	void f(gsl_vector* x, gsl_vector* fx),
	gsl_vector* xstart,
	double dx,
	double epsilon,
	int* n_steps,
	int* n_calls
);

void newton_analytic(
	void f(gsl_vector* x, gsl_vector* fx,gsl_matrix* J),
	gsl_vector* x,
	double dx,
	double epsilon,
	int* n_steps,
	int* n_calls
);


#endif
