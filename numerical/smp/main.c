#include"montecarlo.h"
#include<stdio.h>
#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif
#include<math.h>

double integrand1(gsl_vector* x){
	double x0 = gsl_vector_get(x,0);
	return sqrt(4-x0*x0);
}

double integrand2(gsl_vector* x){
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	return (x0*x0+x1*x1)<1;
}

double integrand3(gsl_vector* x){
	double x0 = gsl_vector_get(x,0);
	double x1 = gsl_vector_get(x,1);
	double x2 = gsl_vector_get(x,2);
	return 1/(M_PI*M_PI*M_PI)*1/(1-cos(x0)*cos(x1)*cos(x2));
}

void test_integrals(){
	printf("--- THIS IS THE SMP VERSION OF THE MONTE CARLO EXERCISE\n\
	It took 15.5s real time to complete the following tasks, and \n\
	It took 41.2s real time to complete in the original exercise\n\n");
	gsl_vector* a1 = gsl_vector_alloc(1);
	gsl_vector* b1 = gsl_vector_alloc(1);
	gsl_vector_set(a1,0,0);
	gsl_vector_set(b1,0,2);
	printf("--- TESTING Plain Monte Carlo Integration ---\n");
	double err;
	int N = 100000000;
	double I = montecarlo_plain(integrand1,a1,b1,N,&err);
	double true_I = M_PI;
	double true_err = fabs(true_I-I);
	printf("Target 1 is sqrt(4-x^2) dx from 0 to 2  =  %g\n",true_I);
	printf("\t I = %g, using %i points, est err. is %g, actual %g\n", I, N, err, true_err);
	gsl_vector* a2 = gsl_vector_alloc(2);
	gsl_vector* b2 = gsl_vector_alloc(2);
	gsl_vector_set(a2,0,-1);
	gsl_vector_set(a2,1,-1);
	gsl_vector_set(b2,0,1);
	gsl_vector_set(b2,1,1);
	I = montecarlo_plain(integrand2,a2,b2,N,&err);
	true_I = M_PI;
	true_err = fabs(true_I-I);
	printf("Target 2 is x^2+y^2 < 1  =  %g\n",true_I);
	printf("\t I = %g, using %i points, est err. is %g, actual %g\n", I, N, err, true_err);
	
	gsl_vector* a3 = gsl_vector_alloc(3);
	gsl_vector* b3 = gsl_vector_alloc(3);
	gsl_vector_set(a3,0,0);
	gsl_vector_set(a3,1,0);
	gsl_vector_set(a3,2,0);
	gsl_vector_set(b3,0,M_PI);
	gsl_vector_set(b3,1,M_PI);
	gsl_vector_set(b3,2,M_PI);
	I = montecarlo_plain(integrand3,a3,b3,N,&err);
	true_I = pow(tgamma(0.25),4)/(4*M_PI*M_PI*M_PI);
	true_err = fabs(true_I-I);
	printf("Target 3 is [1-cos(x)cos(y)cos(z)]/PI^3 dx dy dz  =  %g\n",true_I);
	printf("\t I = %g, using %i points, est err. is %g, actual %g\n", I, N, err, true_err);
	gsl_vector_free(a1);
	gsl_vector_free(b1);
	gsl_vector_free(a2);
	gsl_vector_free(b2);
	gsl_vector_free(a3);
	gsl_vector_free(b3);
}

void test_error(){
	gsl_vector* a = gsl_vector_alloc(2);
	gsl_vector* b = gsl_vector_alloc(2);
	gsl_vector_set(a,0,-1);
	gsl_vector_set(a,1,-1);
	gsl_vector_set(b,0,1);
	gsl_vector_set(b,1,1);
	for(int N = 64; N<1e8; N*=2){
		double err;
		double I = montecarlo_plain(integrand2,a,b,N,&err);
		double true_I = M_PI;
		double true_err = fabs(true_I-I);
		printf("%i\t%g\t%g\t%g\n",N,true_err,err,I);
	}
	gsl_vector_free(a);
	gsl_vector_free(b);
}

int main(int argc, char** argv){
	if(argc > 1){
		test_error();
	}else{
		test_integrals();
	}
	return 0;
}
