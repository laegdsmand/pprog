#include<math.h>
#include<stdlib.h>
#include<assert.h>
#include<time.h>
#include"montecarlo.h"
#include<omp.h>
#define RND ((double) rand_r(seed) / RAND_MAX)
#define N_PARALLEL 10

/* Generates a x_i in [a_i;b_i] */
void random_x(unsigned int* seed, gsl_vector* a, gsl_vector* b, gsl_vector* x){
	int dim = a->size;
	assert(dim == b->size);
	assert(dim == x->size);
	for(int i = 0; i<dim; i++){
		double x_i = gsl_vector_get(a,i) + RND*(gsl_vector_get(b,i)-gsl_vector_get(a,i));
		gsl_vector_set(x, i, x_i);
	}
}

//Plain monte carlo integration of a function f
double montecarlo_plain(
	double f(gsl_vector* x),	//Function to integrate
	gsl_vector* a,			//Lower limit
	gsl_vector* b,			//Upper limit
	int N,				//Number of points
	double* error			//Estimated error
){
	double V = 1;
	double dim = a->size;	
	for(int i = 0; i < dim; i++){
		V *= gsl_vector_get(b,i)-gsl_vector_get(a,i);
	}
	N /= N_PARALLEL;
	double sum[N_PARALLEL], sum_sq[N_PARALLEL];
	#pragma omp parallel for 
	for(int j = 0; j < N_PARALLEL; j++){
		gsl_vector* x = gsl_vector_alloc(dim);
		double fx;
		unsigned int seed = j * time(NULL) + 11;
		sum[j]=0;
		sum_sq[j]=0;
		for(int i = 0; i < N; i++){	//Calculate function value at many random points
			random_x(&seed,a,b,x);
			fx = f(x);
			sum[j] += fx;
			sum_sq[j] += fx*fx;
		}
		//printf("finished running %i\n",j);
		gsl_vector_free(x);
	}
	double avr = 0, var = 0;
	for(int j = 0; j<N_PARALLEL; j++){
		avr += sum[j] / N;
		var += sum_sq[j] / N;
	}
	avr /= N_PARALLEL;			 //Calculate mean function value
	var = var / N_PARALLEL - avr*avr; //Calculate variance
	*error = sqrt(var/(N*N_PARALLEL))*V;			 //Estimate error on integral estimate	
	return avr*V;				 //Integral estimation
}


