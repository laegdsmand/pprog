#ifndef MONTECARLO
#define MONTECARLO
#include<gsl/gsl_vector.h>

double montecarlo_plain(
	double f(gsl_vector* x),	//Function to integrate
	gsl_vector* a,			//Lower limit
	gsl_vector* b,			//Upper limit
	int N,				//Number of points
	double* error			//Estimated error
);

#endif
