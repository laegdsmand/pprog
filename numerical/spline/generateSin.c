#include<stdio.h>
#include<math.h>

int main(){
	double x_min = 0.0, x_max = 2*M_PI;
	int n_points = 16;
	for(int i = 0; i < n_points; i++){
		double x = (x_max-x_min)/n_points * i + x_min;
		printf("%.5g\t%.5g\n",x,sin(x));
	}
	return 0;
}
