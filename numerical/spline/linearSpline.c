#include<stdio.h>
#include<assert.h>
#include<stdlib.h>

void assert_valid_interp(int n, double *x, double z){
	
	//Is input valid
	assert(n>1);
	assert(z>=x[0]);
	assert(z<=x[n-1]);
}

/*Returns a coeff for linear interpolation b/w i'th and i+1'th tabulated point */
double linterp_get_a(int i, double *y){
	return y[i];
}


/*Returns b coeff for linear interpolation b/w i'th and i+1'th tabulated point */
double linterp_get_b(int i, double *x, double *y){
	return (y[i+1]-y[i])/(x[i+1]-x[i]);
}
double linterp(int n, double *x, double*y, double z){
	assert_valid_interp(n,x,z);
	//Do a binary search of z inside x
	int lowerBound = 0, upperBound = n-1;
	while(upperBound-lowerBound > 1) {
		int middle = (upperBound+lowerBound)/2;
		if(z > x[middle]) {
			lowerBound = middle;
		}else{
			upperBound = middle;
		}
	}
	int i = lowerBound;
	//Calculate coefficients
	double a = linterp_get_a(i,y);
	double b = linterp_get_b(i,x,y); 
	return a + b * (z-x[i]);
}

double linterp_integ(int n, double *x, double *y, double z) {
	assert_valid_interp(n,x,z);
	double integral = 0;
	int i = 0;
	while(z > x[i+1]){
		double a = linterp_get_a(i,y);
		double b = linterp_get_b(i,x,y);
		double w = x[i+1]-x[i];
		integral = integral + w*(a+b*w/2);
		i++;
	}
	double b = linterp_get_b(i,x,y);
	double a = linterp_get_a(i,y);
	double w = z - x[i];
	integral = integral + w*(a+b*w/2);
	return integral;
}


int main(int argc, char** argv){
	assert(argc>1);//needs number of lines in file as input arg.
	int n_lines = atoi(argv[1]);
	double x[n_lines];
	double y[n_lines];
	for(int i = 0; i < n_lines; i++){
		scanf("%lg\t%lg",&x[i],&y[i]);
		
	}
	int n_interp_points = 100;
	for(int j = 0; j< n_interp_points; j++){
		double z_j = (x[n_lines-1]-x[0])/n_interp_points*j + x[0];
		double f_j = linterp(n_lines, x, y, z_j);
		double F_j = linterp_integ(n_lines, x, y, z_j);
		printf("%.5g\t%.5g\t%.5g\n",z_j,f_j,F_j);
	}
	return 0;
}

