#include<assert.h>
#include<stdio.h>
#include<stdlib.h>

typedef struct {int n; double *x, *y, *b, *c;} qspline;

int qspline_build_coeffs(qspline *s){
	int i,n=s->n;
	double *x = s->x, *y = s->y, *b = s->b, *c = s->c;
	double h[n-1],p[n-1];//aux arrays containing width of intervals and linear slope
	for(i=0; i < n-1; i++){
		h[i] = x[i+1]-x[i];
		p[i] = (y[i+1]-y[i])/h[i];
	}
	s->c[0] = 0; //start by guessing c_0 = 0
	for(i=0; i<n-2;i++){
		c[i+1] = (p[i+1] - p[i] - c[i]*h[i]) / h[i+1];
	}
	c[n-2] = c[n-2] / 2;
	for(i=n-3; i>=0; i--){
		c[i] = (p[i+1] - p[i] - c[i+1]*h[i+1]) / h[i];
	}
	for(i=0; i<n-1; i++){
		b[i]=p[i] - c[i]*h[i];
	}
	return 0;
}
//Allocates memory for qspline and builds parameters b_i, c_i
qspline* qspline_alloc(int n, double *x, double*y){
	qspline *s = (qspline*) malloc(sizeof(qspline));
	s->n = n;
	s->x = (double*) malloc(n*sizeof(double));
	s->y = (double*) malloc(n*sizeof(double));
	for(int i=0; i<n; i++){
		s->x[i] = x[i];
		s->y[i] = y[i];
	}
	s->b = (double*) malloc((n-1)*sizeof(double));
	s->c = (double*) malloc((n-1)*sizeof(double));
	qspline_build_coeffs(s);
	return s;
}

//finds the interval which z belongs to by binary search
int find_interval(qspline *s, double z){
	
	int lowBound = 0, upBound= s->n - 1;
	while(upBound-lowBound>1){
		int mid = (lowBound+upBound)/2;
		if(z > s->x[mid]){
			lowBound = mid;
		}else{
			upBound = mid;
		}
	}
	return lowBound;
}

double qspline_evaluate(qspline *s, double z){
	int i = find_interval(s,z);
	double dx = z - s->x[i];
	return s->y[i] + s->b[i]*dx + s->c[i]*dx*dx;
}

double qspline_derivative(qspline *s, double z){
	int i = find_interval(s,z);
	double dx = z - s->x[i];
	return s->b[i] + 2*s->c[i]*dx;
}

double qspline_integral(qspline *s, double z){
	int i = 0;
	double *x = s->x, *y = s->y, *b = s->b, *c = s->c, integral = 0;
	while(z > x[i+1]){
		double dx = x[i+1]-x[i];
		integral += dx*y[i] + dx*dx*b[i]/2 + dx*dx*dx*c[i]/3;
		i++;
	}
	double dx = z - x[i];
	integral += dx*y[i] + dx*dx*b[i]/2 + dx*dx*dx*c[i]/3;
	return integral;
}

//Frees memory allocated to qspline.
void qspline_free(qspline *s){
	free(s->x);
	free(s->y);
	free(s->b);
	free(s->c);
	free(s);
}

void fileSpline(int n_lines){
	double x[n_lines];
	double y[n_lines];
	for(int i = 0; i < n_lines; i++){
		scanf("%lg\t%lg",&x[i],&y[i]);
	}
	qspline *s = qspline_alloc(n_lines,x,y);
	fprintf(stderr,"Allocced qspline\n");
	int n_interp_points = 100;
	for(int j = 0; j< n_interp_points; j++){
		double z_j = (x[n_lines-1]-x[0])/n_interp_points*j + x[0];
		double f_j = qspline_evaluate(s, z_j);
		double dfdx = qspline_derivative(s,z_j);
		double F_j = qspline_integral(s,z_j);
		printf("%.5g\t%.5g\t%.5g\t%.5g\n",z_j,f_j,dfdx,F_j);
	}
	qspline_free(s);
}

void printParams(qspline *s){
	for(int i = 0; i < s->n-1; i++){
		printf("a[%i] = %.5g,   b[%i] = %.5g,  c[%i]%.5g\n",
			i, s->y[i], i, s->b[i], i, s->c[i]);
	}
}

void testSpline(){
	int n_lines = 5;
	double x[5] = {1,2,3,4,5}, y[5] = {1,1,1,1,1}; 
	qspline *s = qspline_alloc(n_lines,x,y);
	printParams(s);
		
}

int main(int argc, char **argv){
	if(argc>1){//needs number of lines in file as input arg.
	int n_lines = atoi(argv[1]);
	fileSpline(n_lines);
	}else{
	testSpline();		
	}
	return 0;	
	
}

